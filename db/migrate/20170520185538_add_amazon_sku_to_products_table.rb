class AddAmazonSkuToProductsTable < ActiveRecord::Migration
  def change
    add_column :products, :amazon_sku, :string
  end
end
