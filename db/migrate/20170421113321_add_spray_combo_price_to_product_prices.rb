class AddSprayComboPriceToProductPrices < ActiveRecord::Migration
  def change
    add_column :product_prices, :spray_combo_price, :float
  end
end
