class AddOrderNumberToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :displayable_order_id, :string
    add_column :orders, :generated_order_id, :string
    
    add_column :orders, :fba_response, :text
    
  end
end
