class AddDaysToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :gap_days, :integer, :default=>30
  end
end
