class AddAmazonCustomerField < ActiveRecord::Migration
  def change
    add_column :customers, :amazon_customer, :boolean, default: false
  end
end
