class AddPaypalToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :paypal, :string
  end
end
