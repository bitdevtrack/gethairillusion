class CreateDistributorProductPrices < ActiveRecord::Migration
  def change
    create_table :distributor_product_prices do |t|
      t.integer :distributor_id
      t.integer :distributor_product_id
      t.float :price

      t.timestamps
    end
  end
end
