class AddPaidFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :paid, :boolean, :default => true
  end
end
