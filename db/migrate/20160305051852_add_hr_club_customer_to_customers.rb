class AddHrClubCustomerToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :hr_club_customer, :boolean, :default => false
  end
end
