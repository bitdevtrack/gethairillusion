class AddHcOrdersToOrders < ActiveRecord::Migration
  
  def change
    add_column :orders, :hc_order, :boolean, :default=>false 
  end
  
end
