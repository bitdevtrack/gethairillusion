class AddSmallBottlePrice < ActiveRecord::Migration
  def change
    add_column :product_prices, :small_product_price, :float
  end
end
