class AddCardDeclinedFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :card_error, :boolean, :default => false
    add_column :orders, :warning_sent, :boolean, :default => false
  end
end
