class CreateEbayProducts < ActiveRecord::Migration
  def change
    create_table :ebay_products do |t|
      t.string :name
      t.string :shipwire_sku
      t.string :amazon_sku
      t.integer :quantity

      t.timestamps
    end
  end
end
