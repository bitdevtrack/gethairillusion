class AddRecurrentPriceToProductPrice < ActiveRecord::Migration
  def change
    add_column :products, :recurrent_price, :integer
  end
end
