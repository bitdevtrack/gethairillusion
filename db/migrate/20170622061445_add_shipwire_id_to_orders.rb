class AddShipwireIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :shipwire_id, :integer
  end
end
