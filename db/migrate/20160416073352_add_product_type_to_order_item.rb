class AddProductTypeToOrderItem < ActiveRecord::Migration
  def change
    add_column :order_items, :product_type, :string, :default=>"Product"
  end
end
