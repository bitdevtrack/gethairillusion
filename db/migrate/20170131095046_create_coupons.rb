class CreateCoupons < ActiveRecord::Migration
  def change
    create_table :coupons do |t|
      t.string :name
      t.boolean :active, :default=>true
      t.string :discount_type
      t.float :discount_value

      t.timestamps
    end
  end
end
