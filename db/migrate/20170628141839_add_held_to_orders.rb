class AddHeldToOrders < ActiveRecord::Migration
  def change
  	add_column :orders, :held, :boolean, :default=>false
  end
end
