class AddShippingToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :shipping_price, :float
    add_column :orders, :process_handling_price, :float
    add_column :orders, :agent_id, :integer
  end
end
