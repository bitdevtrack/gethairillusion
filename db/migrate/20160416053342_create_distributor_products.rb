class CreateDistributorProducts < ActiveRecord::Migration
  def change
    create_table :distributor_products do |t|
      t.string :name

      t.timestamps
    end
  end
end
