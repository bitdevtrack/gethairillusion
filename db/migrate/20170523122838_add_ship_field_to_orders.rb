class AddShipFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :ship_from_shipwire, :boolean, default: true
  end
end
