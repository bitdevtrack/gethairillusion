class AddTrackingUpdatedToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :tracking_updated, :boolean, :default=>false
  end
end
