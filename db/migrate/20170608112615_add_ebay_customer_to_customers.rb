class AddEbayCustomerToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :ebay_customer, :boolean, :default=>false
  end
end
