class AddShippingPriceToProductPrices < ActiveRecord::Migration
  
  def change
    add_column :product_prices, :shipping_price, :float, :default=>0
  end
  
end
