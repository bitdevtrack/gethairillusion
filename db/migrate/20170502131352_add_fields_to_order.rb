class AddFieldsToOrder < ActiveRecord::Migration
  def change
    add_column :orders, :status, :string
    add_column :orders, :tracking_number, :string
    add_column :orders, :order_source, :string 
    
  end
end
