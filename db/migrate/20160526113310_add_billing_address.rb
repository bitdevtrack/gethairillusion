class AddBillingAddress < ActiveRecord::Migration
  def change
    add_column :customers, :billing_first_name, :string
    add_column :customers, :billing_last_name, :string
    add_column :customers, :billing_address1, :string
    add_column :customers, :billing_address2, :string
    add_column :customers, :billing_city, :string
    add_column :customers, :billing_state, :string
    add_column :customers, :billing_zip, :string
  end
end
