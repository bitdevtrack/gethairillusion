class AddShippingPhoneToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :shipping_phone, :string
  end
end
