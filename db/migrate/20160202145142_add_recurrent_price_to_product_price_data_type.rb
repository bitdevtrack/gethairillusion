class AddRecurrentPriceToProductPriceDataType < ActiveRecord::Migration
  def change
    change_column :products, :recurrent_price, :float
  end
end
