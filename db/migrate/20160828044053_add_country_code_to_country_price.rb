class AddCountryCodeToCountryPrice < ActiveRecord::Migration
  def change
    add_column :country_prices, :country_code, :string
  end
end
