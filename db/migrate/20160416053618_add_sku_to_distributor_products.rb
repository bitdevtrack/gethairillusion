class AddSkuToDistributorProducts < ActiveRecord::Migration
  def change
    add_column :distributor_products, :sku, :string
  end
end
