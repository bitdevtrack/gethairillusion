class AddVersion2OrdersFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :version_2_order, :boolean, :default => false
  end
end
