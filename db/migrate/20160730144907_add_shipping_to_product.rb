class AddShippingToProduct < ActiveRecord::Migration
  def change
    add_column :products, :shipping_price, :string
  end
end
