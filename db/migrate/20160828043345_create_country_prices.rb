class CreateCountryPrices < ActiveRecord::Migration
  def change
    create_table :country_prices do |t|
      t.string :country
      t.float :price

      t.timestamps
    end
  end
end
