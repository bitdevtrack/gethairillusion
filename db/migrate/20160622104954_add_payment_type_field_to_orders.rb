class AddPaymentTypeFieldToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :payment_type, :string, :default=>"card"
  end
end
