class AddDiscountToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :discount, :float
    add_column :orders, :coupon_code, :string
    add_column :orders, :coupon_id, :integer
  end
  
end
