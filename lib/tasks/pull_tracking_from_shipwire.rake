require 'rest-client'
require 'json'

namespace :hair do

  task :pull_tracking_from_shipwire => :environment do
    puts "shipwire rake started"
    Shipwire.configure do |config|
      config.username = "support@hairillusion.com"
      config.password = "ZZZack!!!"
      config.endpoint = URI::encode('https://api.shipwire.com')
    end 
    
    orders= Order.where("shipwire_id is not null and tracking_number is null")
    
    orders.each do |order|
      response = Shipwire::Orders.new.find(order.shipwire_id)
      puts response.inspect

      if(response.body['status'] == 200)
        begin  
        if response.body['resource']['items'][0]['resource'] 
          tracking = response.body['resource']['items'][0]['resource']['tracking']
          carrierCode = response.body['resource']['items'][0]['resource']['carrierCode']
          order.tracking_number = tracking
          order.shipping_code = carrierCode
          begin
            result = Ebayr.call(:CompleteSale, :OrderID=>order.ebay_order_id, :ShipmentTrackingNumber=>tracking,
              :ShippingCarrierUsed=>order.carrierCode, :Shipped=>true)
              
            if result[:ack] && result[:ack]== "Success"
              order.update_attribute(:tracking_updated, true)
              OrderMailer.send_tracking(order.id).deliver! 
            end
          rescue => error 
              OrderMailer.ebay_tracking_error(order.id).deliver! 
          end

        end
        rescue => error
          OrderMailer.send_shipwire_error("exception during rake task of pulling tracking from shipwire--#{error}",order.id).deliver!
         next
        end 
      end 
    end 
    
  end

end