namespace :hair do
  require "peddler"
  
  task :push_amazon_orders => :environment do 
    client = MWS.fulfillment_outbound_shipment(
      primary_marketplace_id: "ATVPDKIKX0DER",
      merchant_id: "A1D4HR9FQLX93E",
      aws_access_key_id: "AKIAJSFSWTEXHLUC4IJQ",
      aws_secret_access_key: "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",
    ) 
     
  orders = Order.find_by_sql "select o.* from orders o left join customers c on c.id = o.orderer_id where (o.order_source is null or o.order_source != 'amazon') and o.held=false and o.ship_from_shipwire=false and o.orderer_type='Customer' and o.paid=true and o.shipment_id is null and o.version_2_order = true and o.cancelled = false and shipped=false and o.created_at > '2017-05-05'"
  puts orders.size
  Rails.logger.info "push to amazon started: #{orders.size}"
  
  orders.each do |order|  
    puts "xxxxxxxxxxxx"
    customer = order.orderer
    address = {:Name=>customer.billing_first_name + " " + customer.billing_last_name, :Line1=> customer.billing_address1, :Line2=> customer.billing_address2, :City=>customer.billing_city, :StateOrProvinceCode=> customer.billing_state, :PostalCode=>customer.billing_zip, :CountryCode=>customer.billing_country }
 
    order_items = order.order_items
    items = []
    order_items.each do |item| 
      product = Product.find item.product_id  
      if product.amazon_sku.blank?
       # order.update_attribute(:shipped, "failed")   
      else
        item = {:SellerSKU=>product.amazon_sku, :SellerFulfillmentOrderItemId=>product.id.to_s, :Quantity=>item.quantity, :GiftMessage=>'Thanks for your order'}
        items << item 
      end
    end
    
          
    order_id = "fromAdmin-#{order.id}11111"
    displayable_order_id = "fromAdmin-#{order.id}-11111#{order.order_source}"
    puts items.inspect
    puts "....#{order_id}......#{displayable_order_id}."
    puts address.inspect
    
    shipping_category = 'Standard'
    shipping_category = order.shipping_code unless order.shipping_code.blank?
     
    opts = { notification_email_list: [customer.email] }
    begin
      result = client.create_fulfillment_order(order_id, displayable_order_id, DateTime.now.iso8601, 'Added from API', shipping_category, address, items, opts).parse
      OrderDelivery.create(:order_id => order_id, :delivered_date=>Date.today)
      shipment = Shipment.create(:created_at=> Time.now)
      order.update_attribute(:shipment_id, shipment.id)
      order.update_attribute(:last_delivery_date, Date.today)  
      order.update_attribute(:displayable_order_id, displayable_order_id)
      order.update_attribute(:generated_order_id, order_id)
      order.update_attribute(:fba_response, result)
      order.update_attribute(:order_status, "processing")
      order.update_attribute(:shipped, true)
    rescue => error
      puts "XXXXXXXXXXXx"
      puts error.inspect
      OrderMailer.send_amazon_error("exception during rake task of send amazon order creation--#{error}",order.id).deliver!
      
    end 
    Rails.logger.info result.inspect
    puts result.inspect
    
  end 
    
  end 

end