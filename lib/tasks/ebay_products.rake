namespace :hair do
  
  task :insert_ebay_products => :environment do     
[ 
{name: "Hair illusion Hairline Optimizer for Proper Hair Fibers Hair Line", quantity: 1, product_id: 19, ebay_product_id: "141820714654" },
{name: "38.5g LARGE Hair illusion CONCEALER HUMAN HAIR FIBERS Fibers, Black", quantity: 1, product_id: 2, ebay_product_id: "131831974927" },
{name: "Hair illusion  REAL HUMAN HAIR FIBERS Undetectable  Color Black 18g", quantity: 1, product_id: 21, ebay_product_id: "141865922625" },
{name: " Hair Illusion Real Hair Fibers (Black) 18G.", quantity: 1, product_id: 21, ebay_product_id: "141865921557" },
{name: "Hair Illusion Hair Fibers Extensions For Balding Thinning Hair Black 38g", quantity: 1, product_id: 21, ebay_product_id: "141647584333" },
{name: "Hair illusion Thickening Fibers 38.5g Bald Spot Thinning Concealer Hair Fibers[Black,None Of These Below]", quantity: 1, product_id: 2, ebay_product_id: "141755573539" },
{name: " Hair illusion FIBER Hold Spray", quantity: 1, product_id: 9, ebay_product_id: "131501329651" },
{name: "#1 Hair illusion Hair Building Fibers Hair Loss Thickening Fibers Black 3x NEW", quantity: 3, product_id: 2, ebay_product_id: "131947810857" },
{name: "Premium Hairillusion Hair Fibers Hold Spray For Hair Loss Bald Thin Hair", quantity: 1, product_id: 9, ebay_product_id: "131672273379" },
{name: "Large 38.5g Hair Illusion Hair Fibers HAIR LOSS Bald Thinning Hair Dark Brown", quantity: 1, product_id: 3, ebay_product_id: "131713823364" },
{name: "LARGE Thickening Hair Fibers 38.5g Bald Spot Thinning Hair Fibers Concealer NEW[Black,None Of These Below]", quantity: 1, product_id: 2, ebay_product_id: "131831974903" },
{name: "BIG 38.5g Natural Hair Concealer & Thickener Fibers Bald Spot Hair Cover Up  [Hair Fibers Black,Bald Spot Concealer]", quantity: 1, product_id: 2, ebay_product_id: "141891495395" },
].each do |ref|
        EbayProduct.find_or_create_by(ref)
    end 
  end 
end