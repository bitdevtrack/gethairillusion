namespace :hair do
  
  task :upload_tracking => :environment do
    
    require 'ebayr'
    
    Ebayr.dev_id = "55d1ecf9-67b1-4648-aa52-df352e7b0171" 
    
    Ebayr.auth_token = "AgAAAA**AQAAAA**aAAAAA**ea03WQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wJkYanCJmFog2dj6x9nY+seQ**NLkDAA**AAMAAA**Ffoiz+VxgFEkL0qw0ONMsnVkewlUi4ebVQugHlHwzvsaVF4iCc7qyW7fC14XHO7o4t6m5o4YWC5YP8ufdTTmftTvNawCkDoXNj11rUoWshspLoNXbDuhZRMXmgSEtQPlI2ppkvy7tHI5TJWOfFs/qq2MbWJGj7uG7upXpndII5Rpn5oPrQ6XBrmyA3CV8z9mwdqldH2XKfyKB89iKO+A54WoV7/Duxoilj1AiEktrYpnVFCDJMXrgZsf+AIDMtj3sFm8ZWjBrUGy+je/ahiAG8cPcS644wXt3IpBsjTFrszCCUBv5wt3m2nkzlY9hbMq4qW64azQQCwduJWbkH0Y6xg5YyjeFuNrZmd9b46etZ7OQrjqq4ilAdL39GZJDjN9UphcH/tYejrbsHh98pLoUx6db6LbuLnBlZLAfvtEAGh+lRsL233n5u7Cam3XYLaUb6FI0NKMeUvjF3tUZMoMJe8nihGWFMmzbuLsX1NyDRLq5s1cgetRLUZSsJZ+3ijgwpfH5qoNXabTfAAr/UY9V0efWBuyUF9yyTJolL3D1xmMxU3iLrDuv1VGskDONW6ge1LzbxgKXcs5Y6CZi9w3PR6AP/i8I4pim23LGUt+kE2sPrcWTZCgGQqGE3558QJyV1pKGHnRsyFBSNBw7OWHgrISgWPv7/RcAL9YwfGSvLDZ5KFMnw7l9XkHeqSm++ETvdZwj6nYPtZ3vpnUiCsCdD/ZMBPMe9VQ8eZJd50rykna+4uxOdtilLMG9/fV+ZNw"
    Ebayr.site_id = "0"
    token = "AgAAAA**AQAAAA**aAAAAA**ea03WQ**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wJkYanCJmFog2dj6x9nY+seQ**NLkDAA**AAMAAA**Ffoiz+VxgFEkL0qw0ONMsnVkewlUi4ebVQugHlHwzvsaVF4iCc7qyW7fC14XHO7o4t6m5o4YWC5YP8ufdTTmftTvNawCkDoXNj11rUoWshspLoNXbDuhZRMXmgSEtQPlI2ppkvy7tHI5TJWOfFs/qq2MbWJGj7uG7upXpndII5Rpn5oPrQ6XBrmyA3CV8z9mwdqldH2XKfyKB89iKO+A54WoV7/Duxoilj1AiEktrYpnVFCDJMXrgZsf+AIDMtj3sFm8ZWjBrUGy+je/ahiAG8cPcS644wXt3IpBsjTFrszCCUBv5wt3m2nkzlY9hbMq4qW64azQQCwduJWbkH0Y6xg5YyjeFuNrZmd9b46etZ7OQrjqq4ilAdL39GZJDjN9UphcH/tYejrbsHh98pLoUx6db6LbuLnBlZLAfvtEAGh+lRsL233n5u7Cam3XYLaUb6FI0NKMeUvjF3tUZMoMJe8nihGWFMmzbuLsX1NyDRLq5s1cgetRLUZSsJZ+3ijgwpfH5qoNXabTfAAr/UY9V0efWBuyUF9yyTJolL3D1xmMxU3iLrDuv1VGskDONW6ge1LzbxgKXcs5Y6CZi9w3PR6AP/i8I4pim23LGUt+kE2sPrcWTZCgGQqGE3558QJyV1pKGHnRsyFBSNBw7OWHgrISgWPv7/RcAL9YwfGSvLDZ5KFMnw7l9XkHeqSm++ETvdZwj6nYPtZ3vpnUiCsCdD/ZMBPMe9VQ8eZJd50rykna+4uxOdtilLMG9/fV+ZNw"
    
    Ebayr.app_id = "Ronniepa-hairillu-PRD-0090330f6-ddf2fafa"
    
    Ebayr.cert_id = "PRD-090330f65b77-8d4d-44dc-b945-591a"
    
    Ebayr.ru_name = "Ronniepa-hairillu-SBX-0090330f6-0eeab53b"
    
    # Set this to true for testing in the eBay Sandbox (but remember to use the
    # appropriate keys!). It's true by default. 
    Ebayr.sandbox = false  
    
    orders = Order.where("ebay_order_id is not null and tracking_updated=false and tracking_number is not null")
    
    orders.each do |order|
      begin
      result = Ebayr.call(:CompleteSale, :OrderID=>order.ebay_order_id, :ShipmentTrackingNumber=>order.tracking_number,
        :ShippingCarrierUsed=>order.shipping_code, :Shipped=>true)
        
      if result[:ack] && result[:ack]== "Success"
        order.update_attribute(:tracking_updated, true)
        OrderMailer.send_tracking(order.id).deliver! 
      end
      rescue => error
          puts "XXXXXXXXXXXx"
          puts error.inspect
          OrderMailer.ebay_tracking_error(order.id).deliver!
 
      end
    end
    
    Rails.logger.info result.inspect 
    puts result.inspect
    
  end 
  
end
