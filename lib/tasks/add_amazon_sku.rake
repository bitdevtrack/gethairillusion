namespace :hair do
  
  task :add_amazon_sku => :environment do
    
      pp = Product.where(:description=>"Black").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "UC-XH6U-9EMD")
      end
      
      pp = Product.where(:description=>"Jet Black").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "6P-3DC6-J5UC")
      end
      
      pp = Product.where(:description=>"Dark Brown").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "G5-MM2U-TNR5")
      end
      
      pp = Product.where(:description=>"Brown").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "O1-8ND2-EEOI")
      end
      
      pp = Product.where(:description=>"Light Brown").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "T5-3R2L-Q9G8")
      end 
      
      pp = Product.where(:description=>"Auburn").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "Q0-WMO7-AS2S")
      end
      
      pp = Product.where(:description=>"Auburn").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "Q0-WMO7-AS2S")
      end
      
      pp = Product.where(:description=>"Blonde").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "35-S1QU-XESH")
      end
      
      pp = Product.where(:description=>"Light Blonde").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "KH-XBRQ-SNMN")
      end
      
      pp = Product.where(:description=>"Jet Black 18g").first
      unless pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-JB")
      end
      
      pp = Product.where(:description=>"Jet Black 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-JB")
      end
      
      pp = Product.where(:description=>"Black 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-BL")
      end
      
      pp = Product.where(:description=>"Dark Brown 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-DB")
      end
      
      pp = Product.where(:description=>"Brown 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-BR")
      end
      
      pp = Product.where(:description=>"Blonde 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-BD")
      end
      
      pp = Product.where(:description=>"Light Blonde 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-LBD")
      end      
      
      pp = Product.where(:description=>"Light Blonde 18g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "HIFBA18G-LBD")
      end
      
      pp = Product.where(:description=>"Black 6g").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "6GBLACK")
      end
      
      pp = Product.where(:description=>"Spray Applicator").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "SPRY")
      end
      
      pp = Product.where(:description=>"Optimizer").first
      if pp.amazon_sku.blank?
        pp.update_attribute(:amazon_sku, "UZ-CAMR-XC8Q")
      end
      
      
  end
   
 
end
