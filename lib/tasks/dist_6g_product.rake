namespace :hair do

  task :distributor_6g => :environment do

    dp = DistributorProduct.where(:name=>"Black 6g").first
    sp = DistributorProduct.where(:name=>"Spray Applicator").first

    if dp
      Distributor.all.each do |d|
        dpp=DistributorProductPrice.new(:distributor_id=>d.id, :distributor_product_id=>dp.id, :price=>7.5)
        dpp.save

        dpp=DistributorProductPrice.new(:distributor_id=>d.id, :distributor_product_id=>sp.id, :price=>20.00)
        dpp.save
      end
    end 
  end 
end

