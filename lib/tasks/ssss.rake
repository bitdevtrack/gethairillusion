namespace :hair do

  task :set_distributor_6g => :environment do 
         
    dp = DistributorProduct.where(:name=>"Black 6g").first
    if dp 
      
      Distributor.where("id>351").each do |d|  
        dpp=DistributorProductPrice.new(:distributor_id=>d.id, :distributor_product_id=>dp.id, :price=>2.5)
        
        dpp.save
      end
    end 
    
  end 

end
