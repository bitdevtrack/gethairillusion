namespace :hair do

  task :fo => :environment do 
     
    orders = Order.find_by_sql "select o.* from hairillusion_prod.orders o where o.first_delivery_date = o.last_delivery_date  and o.last_delivery_date = o.next_delivery_date and warning_sent=false and o.next_delivery_date <= '2016-07-29'"
    
    orders.each do |o|
      o.next_delivery_date = o.last_delivery_date + o.gap_days.days
      if Date.today > o.next_delivery_date
        o.next_delivery_date = Date.today
        o.save
        puts puts "id-#{o.id}--o.next_delivery_date--#{o.next_delivery_date}"
      end 
    end
  end 

end