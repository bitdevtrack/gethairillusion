namespace :hair do
  
  task :reset_club_orders => :environment do
    
    orders = Order.where("parent_order_id is null and order_type ='recurrent' and next_delivery_date < ? and last_delivery_date < ? and cancelled=false and hc_order=false and card_error = true", Date.today, Date.today)
    Rails.logger.info "Total orders- #{orders.size}" if orders
    
   orders.each do |s|
      s.update_attribute(:card_error, false)
      s.update_attribute(:warning_sent, false)
     s.update_attribute(:next_delivery_date, Date.today)
    end

  end
   
 
end
