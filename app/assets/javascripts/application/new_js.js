function onChangePlan(offer) {   
	$("#shippingDiv").html("");
	$("#shipment_selected").val('');
	$('#bottom_order_div').hide();
	$('#top_order_div').hide();
	$('#shiwpireInfo').show(); 
	$('#termsDiv').hide(); 
		$.ajax({
       type: "GET",
       url: "/clear_all" ,
       data: {type:offer},
       dataType: "script",
    }); 
    
	if (offer == "bonus") {    
		$('#orderType').val("recurrent"); 
		$('#text').show();
		$('#termsDiv').show();   
		$('#shiwpireInfo').hide();  
		$('#top_order_div').show(); 
		$('#ActionQuantity0').val(0);
		$('#ActionQuantity1').val(0);
		$('#ActionQuantity2').val(0);
		$('#ActionQuantity3').val(0);
		$('#ActionQuantity4').val(0);
		$('#ActionQuantity5').val(0);
		$('#ActionQuantity6').val(0);
		$('#ActionQuantity7').val(0);
		$('#ActionQuantity8').val(0); 
	} 
	else if (offer == "one") 
	{ 
		//$("#color-black").addClass('show');
		$('#orderType').val("one_time"); 
		$('#product_selected').val("false"); 
		$('#text').hide(); 
		$('#bottom_order_div').show();  
		$("#text").addClass('hide');
		$("#bottom_order_div").addClass('show');  
	}  
	else if (offer == "two") 
	{ 
		//$("#color-black").addClass('hide');
		$('#orderType').val("one_time_small"); 
		$('#product_selected').val("false"); 
		$('#text').hide(); 
		$('#bottom_order_div').show();  
		$("#text").addClass('hide');
		$("#bottom_order_div").addClass('show');  
	}  
}
 
	function setProductType(type)
	{   
		$("#orderType").val("recurrent"); 
		$('#product_selected').val("true");
		$("#orderType").val("recurrent"); 
		$("#productType").val(type); 
		$.ajax({
          type: "GET",
          url: "/add_product_to_cart?type=recurrent&val="+type,
          dataType: "script",
     	});  
	}

function onChooseCarier(thiss)
{  
	$.ajax({
          type: "GET",
          url: "/set_shipping_values",
          dataType: "script",
          data:{ value: $(thiss).val()}
     	});
}	 

function onChangeQty()
{ 
	jb_quantity = $("#ActionQuantity1").val();
	black_quantity = $("#ActionQuantity2").val();
	dark_brown_quantity = $("#ActionQuantity3").val();
	brown_quantity = $("#ActionQuantity4").val();
	light_brown_quantity = $("#ActionQuantity5").val();
	aubrown_quantity = $("#ActionQuantity6").val();
	blonde_quantity = $("#ActionQuantity7").val();
	light_blonde_quantity = $("#ActionQuantity8").val();
	var type = $('#orderType').val();
	
	$.ajax({
          type: "GET",
          url: "/add_product_to_cart?type=normal&jet_black="+jb_quantity+"&black="+black_quantity+"&dark_brown="+dark_brown_quantity+"&brown="+brown_quantity+"&light_brown="+light_brown_quantity+"&aubrown="+aubrown_quantity+"&blonde="+blonde_quantity+"&light_blonde="+light_blonde_quantity+"&product_type="+type ,
          dataType: "script",
     	}); 
}

function onClickAgentOrder(e,cart)
{    
	if($('#product_selected').val() == "false")
	{
		alert("Please select product");
		e.preventDefault();
		e.stopPropagation(); 
		return;
	}
	error = ""
	if (!$("#agent_id").val())
	{
		error = error + "Please choose your agent name.\n" 
	}
	if (!$("#CardNumber").val())
	{
		error = error + "Please enter card number.\n" 
	}
	if (!$("#CardCvv2").val())
	{
		error = error + "Please enter CCV number.\n" 
	}
	if (!$("#first_name").val())
	{
		error = error + "Please enter First Name.\n" 
	}
	if (!$("#last_name").val())
	{
		error = error + "Please enter Last Name.\n" 
	}
	if (!$("#address1").val())
	{
		error = error + "Please enter Address.\n" 
	}
	if (!$("#email").val())
	{
		error = error + "Please enter Email Address.\n" 
	} 
	if (!$("#cb_country").val())
	{
		error = error + "Please select Country.\n" 
	}
	if (!$("#country_state").val())
	{
		error = error + "Please select State.\n" 
	}
	if (!$("#city").val())
	{
		error = error + "Please enter City.\n" 
	}
	if (!$("#zip").val())
	{
		error = error + "Please enter Zip Code.\n";
	}
	if (!$("#phone").val())
	{
		error = error + "Please enter Phone.\n" ;
	}
	
	if(document.getElementById("billingCB").checked)
	{
		if (!$("#billing_first_name").val())
		{
			error = error + "Please enter Billing First Name.\n" 
		}
		billing_first_name = $("#billing_first_name").val()
		billing_last_name = $("#billing_last_name").val()
		if (!$("#billing_last_name").val())
		{
			error = error + "Please enter Billing Last Name.\n" 
		}
		if (!$("#billing_address1").val())
		{
			error = error + "Please enter Billing Address.\n" 
		} 
		
		billing_address1 = $("#billing_address1").val()
		billing_address2 = $("#billing_address2").val()
		
		if (!$("#billing_country").val())
		{ 
			error = error + "Please select Billing Country.\n" 
		}
		billing_country = $("#billing_country").val()
		if (!$("#billing_country_state").val())
		{
			error = error + "Please select Billing State.\n" 
		}
		billing_state= $("#billing_country_state").val()
		
		if (!$("#billing_city").val())
		{
			error = error + "Please enter Billing City.\n" 
		}
		billing_city= $("#billing_city").val()
		if (!$("#billing_zip").val())
		{
			error = error + "Please enter Billing Zip Code.\n";
		}
		billing_zip = $("#billing_zip").val()
	}
	
	if(error == "")
	{
		document.getElementById("submit_button").src = "assets/Processing.gif"; 
		$("#AcceptOfferButton").hide();
			$.ajax({
          type: "post",
          url: "/create_agent_order",
          data:{ first_name: $("#first_name").val(), last_name: $("#last_name").val(), address1:$("#address1").val(), 
          		 address2:$("#address2").val(), email:$("#email").val(), cb_country: $("#cb_country").val(), order_state: $("#country_state").val(),
          		 city: $("#city").val(), zip:$("#zip").val(), phone:$("#phone").val(), card_number:$("#CardNumber").val(), 
          		 cvv:$("#CardCvv2").val(), month:$("#CardExpirationMonth").val(), exp_year:$("#CardExpirationYear").val(), agent_id:$("#agent_id").val(), 
          		 eh:$("#eh").val(), billing_first_name:billing_first_name,
	          		 billing_last_name:billing_last_name, billing_address1:billing_address1,
	          		 billing_address2:billing_address2, billing_country:billing_country,
	          		 billing_state:billing_state, billing_city:billing_city,billing_zip:billing_zip}
     	}); 
	}
	else
	{
		alert(error);
	}  	
}

function onClickOrder(e)
{       
	 	
 	order_type = document.getElementById("orderType").value; 
	product_selected = document.getElementById("product_selected").value; 	 
	if(product_selected != "true" )
	{
		alert("Please select product");
		e.preventDefault();
		e.stopPropagation(); 
		return;
	}  
	if(order_type != "recurrent" && $("#shipment_selected").val() != "true")
	{
		alert("Please choose shipping carrier");
		e.preventDefault();
		e.stopPropagation(); 
		return;		
	}
	error = ""; 
	credit_card_number = document.getElementById("CardNumber").value;
	if (!credit_card_number)
	{
		error = error + "Please enter card number.\n";
	}
	ccv_value = document.getElementById("CardCvv2").value;
	if (!ccv_value)
	{
		error = error + "Please enter CCV number.\n";
	}
	first_name = document.getElementById("first_name").value;
	if (!first_name)
	{
		error = error + "Please enter First Name.\n";
	}
	last_name = document.getElementById("last_name").value;
	if (!last_name)
	{
		error = error + "Please enter Last Name.\n";
	}
	address1 = document.getElementById("address1").value;
	address2 = document.getElementById("address2").value;
	
	if (!address1)
	{
		error = error + "Please enter Address.\n";
	}
	email = document.getElementById("email").value;
	if (!email)
	{
		error = error + "Please enter Email Address.\n";
	} 
	country = document.getElementById("cb_country").value;
	if (!country)
	{
		error = error + "Please select Country.\n";
	}
	
	state = document.getElementById("country_state").value;
	if (!state)
	{
		error = error + "Please select State.\n";
	}
	city = document.getElementById("city").value;
	if (!city)
	{
		error = error + "Please enter City.\n";
	}
	zip = document.getElementById("zip").value;
	if (!zip)
	{
		error = error + "Please enter Zip Code.\n";
	}
	phone = document.getElementById("phone").value;
	if (!phone)
	{
		error = error + "Please enter Phone.\n" ;
	}
 	
 	exp_month = document.getElementById("CardExpirationMonth").value;
 	exp_year = document.getElementById("CardExpirationYear").value;
 	
 	billing_first_name = ""
 	billing_last_name = ""
 	billing_address1 = ""
 	billing_address2 = ""
 	billing_country = ""
 	billing_state = ""
 	billing_city = ""
 	billing_zip = ""
 	
 	if(document.getElementById("billingCB").checked)
	{
		if (!$("#billing_first_name").val())
		{
			error = error + "Please enter Billing First Name.\n" 
		}
		billing_first_name = $("#billing_first_name").val()
		billing_last_name = $("#billing_last_name").val()
		if (!$("#billing_last_name").val())
		{
			error = error + "Please enter Billing Last Name.\n" 
		}
		if (!$("#billing_address1").val())
		{
			error = error + "Please enter Billing Address.\n" 
		} 
		
		billing_address1 = $("#billing_address1").val()
		billing_address2 = $("#billing_address2").val()
		
		if (!$("#billing_country").val())
		{
			error = error + "Please select Billing Country.\n" 
		}
		billing_country = $("#billing_country").val()
		if (!$("#billing_country_state").val())
		{
			error = error + "Please select Billing State.\n" 
		}
		billing_state= $("#billing_country_state").val()
		
		if (!$("#billing_city").val())
		{
			error = error + "Please enter Billing City.\n" 
		}
		billing_city= $("#billing_city").val()
		if (!$("#billing_zip").val())
		{
			error = error + "Please enter Billing Zip Code.\n";
		}
		billing_zip = $("#billing_zip").val()
	}

 	
	if(error == "")
	{ 
		order_type = document.getElementById("orderType").value;
		terms = !document.getElementById("terms").checked;
		if( order_type == "recurrent" && terms )
		{
			alert("Please read terms & checkmark box before placing order."); 
		}
		else
		{ 
			document.getElementById("submit_button").style.height = "150px"; 
			document.getElementById("submit_button").style.margin = "-10px"; 
			document.getElementById("submit_button").style.backgroundImage = "url('assets/Processing.gif')"; 	   
			$.ajax({
	          type: "post",
	          url: "/create_order",
	          data:{ first_name: first_name, last_name: last_name, address1: address1, 
	          		 address2: address2, email: email, cb_country: country, order_state: state,
	          		 city: city, zip: zip, phone: phone, card_number: credit_card_number, cvv: ccv_value, 
	          		 month: exp_month, exp_year: exp_year, billing_first_name:billing_first_name,
	          		 billing_last_name:billing_last_name, billing_address1:billing_address1,
	          		 billing_address2:billing_address2, billing_country:billing_country,
	          		 billing_state:billing_state, billing_city:billing_city,billing_zip:billing_zip}
	     	}); 
	     }
	}
	else
	{
		alert(error);
	}  
}

function onChangeDQty(distributor_id){
	
	params = []
	$.each($(".order-quantity"), function(index, value)
		{
			params.push( { product_id: $(this).attr('id'), qty: $(this).val()}) 
	});
	
	$.ajax({
	          type: "get",
	          url: "/get_total_price",
	          data:{ distributor_id: distributor_id, products: params}
	     	});
}

function onChangeBillingCB()
{
	checked = document.getElementById("billingCB").checked;
 
		$.ajax({
	          type: "get",
	          url: "/show_hide_billing_details",
	          data:{ value: checked}
	     	}); 
}


function OnCardUpdate(e)
{
	error = ""  
	
	if( !$("#customer_name").val() ) 
	{
		error = error + "Please enter card holder name.\n"  
	}
	if( !$("#card_number").val() ) 
	{
		error = error + "Please enter card number.\n"  
	}
	if( !$("#exp_month").val() ) 
	{
		error = error + "Please enter card exp month.\n"  
	}
	else
	{
		if( $("#exp_month").val() >12 ||  $("#exp_month").val()== 0 ) 
		{
			error = error + "Please enter valid card exp month.\n"  
		}
	}
	if( !$("#exp_year").val() ) 
	{
		error = error + "Please enter card exp year.\n"  
	}
	else
	{
		if( $("#exp_year").val() <2016 ) 
		{
			error = error + "Please enter valid card exp year.\n"  
		}
	}
	if( !$("#cvc").val() ) 
	{
		error = error + "Please enter card CVC.\n"  
	}
	e.preventDefault();
	e.stopPropagation(); 
	if(error.length>0)
	{
		alert(error);
	}
	else
	{
		$.ajax({
	          type: "post",
	          url: "/users/update_card_details",
	          data:{ name: $("#customer_name").val(), number: $("#card_number").val(), exp_month:$("#exp_month").val(), cvc: $("#cvc").val(), exp_year:$("#exp_year").val()}
	     	});
	} 
}

function onLogout(e)
{
	e.preventDefault();
	e.stopPropagation(); 
	$.ajax({
	          type: "post",
	          url: "/users/logout"
	}); 
}

function getShippingMethods(e)
{
	e.preventDefault();
	e.stopPropagation();   
	product_selected = document.getElementById("product_selected").value; 	 
	if(product_selected != "true" )
	{
		alert("Please select product");
		e.preventDefault();
		e.stopPropagation(); 
		return;
	} 
	error = ""; 
	 
	first_name = document.getElementById("first_name").value;
	if (!first_name)
	{
		error = error + "Please enter First Name.\n";
	}
	last_name = document.getElementById("last_name").value;
	if (!last_name)
	{
		error = error + "Please enter Last Name.\n";
	}
	address1 = document.getElementById("address1").value;
	address2 = document.getElementById("address2").value;
	
	if (!address1)
	{
		error = error + "Please enter Address.\n";
	}
	email = document.getElementById("email").value;
	if (!email)
	{
		error = error + "Please enter Email Address.\n";
	} 
	country = document.getElementById("cb_country").value;
	if (!country)
	{
		error = error + "Please select Country.\n";
	}
	
	state = document.getElementById("country_state").value;
	if (!state)
	{
		error = error + "Please select State.\n";
	}
	city = document.getElementById("city").value;
	if (!city)
	{
		error = error + "Please enter City.\n";
	}
	zip = document.getElementById("zip").value;
	if (!zip)
	{
		error = error + "Please enter Zip Code.\n";
	}
	phone = document.getElementById("phone").value;
	if (!phone)
	{
		error = error + "Please enter Phone.\n" ;
	} 
 	
 	billing_first_name = ""
 	billing_last_name = ""
 	billing_address1 = ""
 	billing_address2 = ""
 	billing_country = ""
 	billing_state = ""
 	billing_city = ""
 	billing_zip = ""
 	
 	if(document.getElementById("billingCB").checked)
	{
		if (!$("#billing_first_name").val())
		{
			error = error + "Please enter Billing First Name.\n" 
		}
		billing_first_name = $("#billing_first_name").val()
		billing_last_name = $("#billing_last_name").val()
		if (!$("#billing_last_name").val())
		{
			error = error + "Please enter Billing Last Name.\n" 
		}
		if (!$("#billing_address1").val())
		{
			error = error + "Please enter Billing Address.\n" 
		} 
		
		billing_address1 = $("#billing_address1").val()
		billing_address2 = $("#billing_address2").val()
		
		if (!$("#billing_country").val())
		{
			error = error + "Please select Billing Country.\n" 
		}
		billing_country = $("#billing_country").val()
		if (!$("#billing_country_state").val())
		{
			error = error + "Please select Billing State.\n" 
		}
		billing_state= $("#billing_country_state").val()
		
		if (!$("#billing_city").val())
		{
			error = error + "Please enter Billing City.\n" 
		}
		billing_city= $("#billing_city").val()
		if (!$("#billing_zip").val())
		{
			error = error + "Please enter Billing Zip Code.\n";
		}
		billing_zip = $("#billing_zip").val()
	}
	
	if(error == '')
	{
		$('#shipButtton').text("Fetching..Wait..");
		$('#shipButtton').prop('disabled', true);
			
		$.ajax({
	          type: "get",
	          url: "/get_shipping",
	          data:{ first_name: first_name, last_name: last_name, address1: address1, 
	          		 address2: address2, email: email, cb_country: country, order_state: state,
	          		 city: city, zip: zip, phone: phone,billing_first_name:billing_first_name,
	          		 billing_last_name:billing_last_name, billing_address1:billing_address1,
	          		 billing_address2:billing_address2, billing_country:billing_country,
	          		 billing_state:billing_state, billing_city:billing_city,billing_zip:billing_zip}
	     	});
	}
	else
	{
		alert(error);
	} 
}

function OnchangeBillingAddress()
{
	if(document.getElementById("billingCB").checked == false )
	{
		$("#shippingDiv").html("");
		$("#shipment_selected").val("");
	} 
}

function OnchangeShippingAddress()
{
	$("#shippingDiv").html("");
	$("#shipment_selected").val(""); 
}

function adminShippingCarrieClick(event)
{
	if (parseInt($("#totalPrice").val()) < 150) {
				alert("Wholesale order must be atleast $150 worth of combine product");
				event.preventDefault();
			} 
			else
			{ 
				document.getElementById("get_shipping").value = true; 
				document.getElementById("distShipButton").value = "Fetching..Please Wait..."; 
			}
}

function onChooseDCarier(thiss)
{  
	$.ajax({
          type: "GET",
          url: "/set_setting_carriers",
          dataType: "script",
          data:{ value: $(thiss).val()}
     	});
}


 