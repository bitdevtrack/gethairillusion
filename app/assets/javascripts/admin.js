// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require ckeditor-jquery
//= require bootstrap.min
//= require application/orders
//= require_tree ./admin/
//= require bootstrap-datepicker


function onUpdatePrice(e)
{
	price = $("#new_price").val();
	
	recurrent_price = $("#recurrent_new_price").val();
	small_price = $("#small_product_price").val();
	shipping_price = $("#shipping_price").val();
	
	valuepack2_price = $("#valuepack_2_price").val(); 
	valuepack3_price = $("#valuepack_3_price").val();
	valuepack4_price = $("#valuepack_4_price").val();
	valuepack5_price = $("#valuepack_5_price").val(); 
	combo_price = $("#combo_price").val(); 
	
	valuepack2_shipping_price = $("#valuepack_2_shipping_price").val(); 
	valuepack3_shipping_price = $("#valuepack_3_shipping_price").val();
	valuepack4_shipping_price = $("#valuepack_4_shipping_price").val();
	valuepack5_shipping_price = $("#valuepack_5_shipping_price").val(); 
	combo_shipping_price = $("#combo_shipping_price").val(); 
	
	spray_price = $("#spray_price").val(); 
	water_price = $("#water_price").val(); 
	mirror_price = $("#mirror_price").val(); 
	opt_price = $("#optimizer_price").val(); 
	combo2_price = $("#combo2_price").val(); 
	combo3_price = $("#combo3_price").val(); 
	laser_comb_price = $("#laser_comb_price").val(); 
	spray_combo_price = $("#applicator_combo_price").val(); 
	
	black6g_price = $("#black_6g_price").val(); 
	applicator_price = $("#applicator_price").val(); 
	
	
	if(price && recurrent_price)
	{
		$.ajax({
          url: "/admin/orders/update_price",
          data: { 
              price: price, recurrent_price:recurrent_price, small_product_price:small_price, shipping_price:shipping_price,
              valuepack2_price:valuepack2_price, valuepack3_price:valuepack3_price, valuepack4_price:valuepack4_price,
              valuepack5_price:valuepack5_price, combo_price:combo_price, valuepack2_shipping_price:valuepack2_shipping_price,
              valuepack3_shipping_price:valuepack3_shipping_price, valuepack4_shipping_price:valuepack4_shipping_price,
              valuepack5_shipping_price:valuepack5_shipping_price, combo_shipping_price:combo_shipping_price, spray_combo_price:spray_combo_price,
              spray_price:spray_price, water_price:water_price, mirror_price:mirror_price, opt_price:opt_price, combo2_price:combo2_price,
              combo3_price:combo3_price, laser_comb_price:laser_comb_price, black6g_price:black6g_price, applicator_price:applicator_price
          }
      });
	}
	else
	{
		alert("Please enter price");
	}
	
	e.preventDefault();
	e.stopPropagation(); 
}
