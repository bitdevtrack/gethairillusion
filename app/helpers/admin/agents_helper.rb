module Admin::AgentsHelper
  
  def get_product_names(order)
    order_items = order.order_items
    name = ""
    order_items.each do |o_i|
      if name == ""
        name = o_i.product.description
      else
        name += ", "+o_i.product.description
      end
    end
    return name
  end
  
  def get_start_date_value
    
    if params[:search]
      if params[:search][:start_date] 
        return params[:search][:start_date].to_date.strftime('%d/%m/%Y') 
      end
    end
    return DateTime.now.to_date.strftime('%d/%m/%Y')
  end

  def end_date_value
    if params[:search]
      if params[:search][:end_date]
        return params[:search][:end_date].to_date.strftime('%d/%m/%Y') rescue DateTime.now.to_date.strftime('%d/%m/%Y')
      end
    end
    return DateTime.now.to_date.strftime('%d/%m/%Y')
  end
  
  def get_order_type
    if params[:search]
      if params[:search][:order_type]
        return params[:search][:order_type] rescue ''
      end
    end
    return ''
  end
  
  def get_order_status
    if params[:search]
      if params[:search][:status]
        return params[:search][:status] rescue ''
      end
    end
    return ''
  end
  
  def get_first_name_value
    if params[:search]
      if params[:search][:first_name]
        return params[:search][:first_name] rescue ''
      end
    end
    return ''
  end
  
  def get_last_name_value
    if params[:search]
      if params[:search][:last_name]
        return params[:search][:last_name] rescue ''
      end
    end
    return ''
  end

  def get_email_value
    if params[:search]
      if params[:search][:email]
        return params[:search][:email] rescue ''
      end
    end
    return ''
  end
  
  def get_price(order_id)
    order = Order.find order_id 
    unless order.shopify_order_id.blank?  
      shop_url = "https://a98b179d72117d149e44ae83796e4c64:b62b5da657f75898e1d45eb6a6e0e247@hair-illusion-llc.myshopify.com/admin"
      ShopifyAPI::Base.site = shop_url   
      begin
        order = ShopifyAPI::Order.find(order.shopify_order_id)  
        return "#{order.total_price} -shopify"
      rescue
        return "cant refund"
      end
    end
    begin
      return (order.stripe_charge[:amount]/100.00).to_f 
    rescue
      return "no charge found in stripe"
    end
    
  end
   
end
