class Order < ActiveRecord::Base
  extend GridTable

  attr_accessor :order_id 
  has_many :distributor_orders, foreign_key: "order_id"  

  belongs_to :orderer, polymorphic: true
  belongs_to :agent
  belongs_to :shipment
  has_many :order_items, dependent: :destroy

  accepts_nested_attributes_for :order_items

  grid_table_control :id
  grid_table_control :created_at
  grid_table_control :orderer
  grid_table_control :host

  scope :unshipped, -> { where(shipment_id: nil).where(refunded_at: nil) }

  def total
    total = order_items.collect { |oi| oi.total }.inject { |sum, x| sum + x } 
    total + shipping_cost
  end
  
  def order_source
    if self.host.blank?
      if self.agent_id
        agent = Agent.find self.agent_id
        return agent.name if agent
        return "" 
      end
    else
      return self.host
    end
    return "" 
  end
  
  def self.create_update_ebay_order(order_hash) 
    
    transaction = nil
    items = []
    
    if order_hash["transaction_array"]["transaction"].kind_of?(Array) 
      transaction = order_hash["transaction_array"]["transaction"][0] 
      order_hash["transaction_array"]["transaction"][0].each_pair do |key, value|  
      end  
      items << transaction["item"]
    else
      transaction = order_hash["transaction_array"]["transaction"]
      items << transaction["item"]
    end 
    
    customer_email = transaction["buyer"]["email"] 
    shipping_address= order_hash["shipping_address"] 
    puts shipping_address.inspect
    
    unless shipping_address["name"].nil? 
      name = shipping_address["name"].split(" ") 
      first_name = name[0]
      if name.size > 1
        last_name = name[1]
      else
        last_name = name[0]
      end 
      
      address1 = shipping_address["street1"]
      address2 = shipping_address["street2"]
      city = shipping_address["city_name"] 
      state = shipping_address["state_or_province"]
      country = shipping_address["country"]
      phone = shipping_address["phone"]
      zip = shipping_address["postal_code"]
      
      state = city if state.nil? 
      zip = city if zip.nil?
      
      customer = Customer.where(:email=>customer_email).first
       
      if customer_email == "Invalid Request"
        customer_email = first_name.to_s + last_name.to_s + "@hairillusion.com"
      end
      
      unless customer
        customer = Customer.new(:ebay_customer=>true, :first_name=>first_name, :last_name=>last_name, 
          :email=>customer_email, :address1=>address1, :address2=>address2, :city=>city,
          :state=>state, :zip=>zip, :country=>country, :hr_club_customer=>false, :phone=>phone,
          :billing_first_name=>first_name, :billing_last_name=>last_name, 
          :billing_address1=>address1, :billing_address2=>address2, 
          :billing_city=>city, :billing_state=>state, :billing_country=>country, :billing_zip=>zip,
          :shipping_phone=>phone)
          
          customer.save!
      else
        customer.update_attributes(:ebay_customer=>true, :first_name=>first_name, :last_name=>last_name, 
          :email=>customer_email, :address1=>address1, :address2=>address2, :city=>city,
          :state=>state, :zip=>zip, :country=>country, :hr_club_customer=>false, :phone=>phone,
          :billing_first_name=>first_name, :billing_last_name=>last_name,:billing_address1=>address1, 
          :billing_address2=>address2, 
          :billing_city=>city, :billing_state=>state, :billing_country=>country, :billing_zip=>zip,
          :shipping_phone=>phone)
      end
      
      order_id = order_hash["order_id"]  
      order = Order.where(:ebay_order_id=>order_id).first
        
      unless order
        shipping_price = transaction["actual_shipping_cost"].to_f  
        paid = false 
        
        begin
          paid = true if order_hash["checkout_status"]["ebay_payment_status"] == "NoPaymentFailure"
        rescue 
        end
   
        order = Order.create(:orderer_id=>customer.id, :orderer_type=>'Customer', :created_at=>order_hash["created_time"].to_date,
              :host=>"ebay", :shipping_price=>shipping_price, :process_handling_price=>0, :version_2_order=>true,
              :paid=>paid, :payment_type=>order_hash["payment_method"], :status=>order_hash["order_status"],
              :order_source=>"ebay", :ebay_order_id=>order_id)
          
              
        items.each do |item|

           ebay_product = nil
           if item.kind_of?(Array)  
             ebay_product = EbayProduct.where(:ebay_product_id=>item[1]).first
           else
             ebay_product = EbayProduct.where(:ebay_product_id=>item["item_id"]).first
           end
          
           puts ebay_product.inspect
          
          if ebay_product && !ebay_product.quantity.blank?
            puts transaction.inspect
            quantity = transaction["quantity_purchased"].to_i 
            quantity = quantity * ebay_product.quantity
            amount_per_bottle = (order_hash["amount_paid"].to_f- shipping_price)/quantity
            puts amount_per_bottle
            orderItem = OrderItem.new(:order_id=>order.id, :quantity=>quantity, :product_id=>ebay_product.product_id, :price=> amount_per_bottle*100, :product_type=>"Product")
            orderItem.save(:validate=>false) 
            OrderMailer.new_ebay_order(order.id).deliver! 
          else
            puts "XXXXXXXXXXXXx--#{item.inspect}"
            e = EbayProduct.where(:ebay_product_id=>item["item_id"]).first
            unless e
              e = EbayProduct.create(:ebay_product_id=>item["item_id"], :name=>item["title"]) if item["title"]
            end
            order.destroy!
            OrderMailer.ebay_notificaton(e.id).deliver!
          end
        end
      end
      
      order.update_shipping_and_tracking(transaction) if order
    end
  end
  
  def update_shipping_and_tracking(order_hash)
    if self.tracking_number.nil?
      if order_hash["shipping_details"]["shipment_tracking_details"]
        self.shipping_code = order_hash["shipping_details"]["shipment_tracking_details"]["shipping_carrier_used"]
        self.tracking_number = order_hash["shipping_details"]["shipment_tracking_details"]["shipment_tracking_number"]
        self.shipped = true
        if self.shipment_id.nil?
          shipment = Shipment.new
         # shipment.save
         # self.shipment_id = shipment.id 
        end
        self.save
      end
    end     
    self.update_attribute(:ship_from_shipwire, !self.can_ship_from_amazon)  
    if self.ship_from_shipwire == false && self.shipping_code.blank?
      self.shipping_code = "Standard"
    end
    self.save!
  end
  
  def get_total_shipping(shipping_price)  
    price = 0.0
    product_count = 0
    self.order_items.each do |s|
      product = Product.find s.product_id 
      if product && product.description != "Mirror" && product.description != "Optimizer" && product.description != "Hair Illusion Fiber Hold Spray"
        product_count+= s.quantity
      end
    end 
    return shipping_price+product_count-1
  end
  
  def total_price
    return 0 if order_items.nil?
    total = order_items.collect { |oi| oi.total }.inject { |sum, x| sum + x } 
    return 0 if total.nil?
    total = total/100.00
  end
  
  def total_commission_price
    hold_product = Product.where(:description=>"Hair Illusion Fiber Hold Spray").last    
    order_items_list = self.order_items.delete_if {|item| item.product_id == hold_product.id } unless self.order_items.empty?  
    if order_items_list.nil? || order_items_list.size == 0
      total = 0
    else
      total = order_items_list.collect { |oi| oi.total }.inject { |sum, x| sum + x } 
      total = total/100.00
    end 
  end

  def total_quantity
    order_items.collect { |oi| oi.quantity }.inject { |sum, x| sum + x } || 0
  end

  def description
    order_items.collect { |oi| "#{oi.product.description} x#{oi.quantity}" if oi.quantity > 0 }.join(', ')
  end

  # total weight in oz
  def total_weight
    order_items.collect { |oi| oi.weight }.inject { |sum, x| sum + x } || 0
  end
  
  def get_shipping_cost
    return 0 if self.order_items.nil? 
    begin
    tax = self.order_items.collect { |oi| oi.tax }.inject { |sum, x| sum + x } 
    return 0 if tax.nil?
    total = tax/100.00
    rescue
      return self.shipping_price.to_f
    end
  end

  def shipping_cost
    total = 0

    # Medium priority flat rate $11.30 for up to 30 bottles.
    medium = 1130

    # Large priority flat rate $15.80 for 30-50 bottles.
    large = 1580

    if orderer_type == Distributor.name
      quantity = total_quantity
      total = (quantity / 50) * large

      if (quantity % 50) <= 30
        total = total + medium
      else
        total = total + large
      end
    end

    total
  end

  def mail_class
    'priority'
  end

  def do_fulfillment 
    logger.info ".........................."
    #order = Shipwire::Orders.new
    orders = Shipwire::Orders.new
    test = orders.list
    p test
    #add shipwire post data
    params = { "expand"=>{
        "externalId" => '333'
      }
    }
    p orders
    logger.info ".........................."
  end
  def mail_piece
    if total_quantity <= 30
      'flat rate priority box'
    else
      'large flat rate priority box'
    end
  end

  def stripe_charge
    Stripe::Charge.retrieve(self.stripe_id)
  end

  def prepare_for_shipping
    results = []

    order = shipping_order(self)

    self.order_items.each do |order_item|
      if (order.total_quantity + order_item.quantity <= 50)
        order.order_items<< OrderItem.new(product: order_item.product, price: order_item.price, quantity: order_item.quantity)
      else
        partial_quantity = 50 - order.total_quantity

        if partial_quantity > 0
          order.order_items<< OrderItem.new(product: order_item.product, price: order_item.price, quantity: partial_quantity)
        end

        results<< order
        order = shipping_order(self)

        order_item_quantity = order_item.quantity
        order_item_quantity -= partial_quantity

        while order_item_quantity > 0
          if order_item_quantity > 50
            order.order_items<< OrderItem.new(product: order_item.product, price: order_item.price, quantity: 50)
            order_item_quantity -= 50

            results<< order
            order = shipping_order(self)
          else
            order.order_items<< OrderItem.new(product: order_item.product, price: order_item.price, quantity: order_item_quantity)
            order_item_quantity = 0
          end
        end
      end
    end

    if order.total_quantity > 0
      results<< order
    end

    results
  end
  
  def self.save_order(row)   
 
    orders =  HCOrder.where("email=?",row["Email"])     
    order_date = Date.strptime(row['Order Date'],"%m/%d/%y") 
 
    if orders.size > 0
      
      customer = Customer.where(:email=>row["Email"]).first
      if customer
        logger.info row["Email"]
        customer.hr_club_customer = true
        customer.first_name = row["Shipping First Name"]
        customer.last_name = row["Shipping Last Name"]
        customer.address1 = row["Shipping Street Line 1"]
        customer.address2 = row["Shipping Street Line 2"]
        customer.city = row["Shipping City"]
        customer.state = row["Shipping State"] 
        customer.zip = row["Shipping Zip"]
        customer.country = row["Shipping Country"]
        customer.phone = row["Phone"]
      else
        customer=Customer.new(:hr_club_customer=>true, :first_name=>row["Shipping First Name"], :last_name=>row["Shipping Last Name"],
            :email=>row["Email"], :address1=>row["Shipping Street Line 1"], :address2=>row["Shipping Street Line 2"], :city=>row["Shipping City"],
            :state=>row["Shipping State"], :zip=>row["Shipping Zip"], :country=>row["Shipping Country"], :phone=>row["Phone"])
      end
       
      if customer.save 
        card = CustomerCard.new(:card_name=>customer.name, :card_number=>row["CardNumber"], :ccv=>row["Cvv2 Number"], :exp_month=>row["Card Expiration"].split("/")[0], :exp_year=>row["Card Expiration"].split("/")[1], :customer_id=>customer.id)
        card.save  
        
        order = Order.new(:order_type=>'recurrent',:hc_order=>true, :host=>"buyhairillusion old order", :version_2_order=>true,:orderer_type=>"Customer", :orderer_id=>customer.id, :first_delivery_date=>order_date, :created_at=>order_date.to_datetime)
        order.ship_from_shipwire = false 
        
        if order.save 
          product_name = "" 
          unless row['Product Sku 1'].blank?
            product = HCProduct.where("id=?",row['Product Sku 1']).first
            Order.save_product(order, product.name) if product
          end
      
          unless row['Product Sku 2'].blank?
            product = HCProduct.where("id=?",row['Product Sku 2']).first
            Order.save_product(order, product.name) if product
          end
      
          unless row['Product Sku 3'].blank?
            product = HCProduct.where("id=?",row['Product Sku 3']).first 
            Order.save_product(order, product.name) if product
          end
      
          unless row['Product Sku 4'].blank?
            product = HCProduct.where("id=?",row['Product Sku 4']).first 
            if product
              Order.save_product(order, product.name) 
            end
          end    
        end  
      end 
    end
  end
  
  def self.save_product(order, product_name)
    product = nil 
    if product_name == "BROWN HAIR ILLUSION CLUB"
        product = Product.where(:description=>"Brown", :product_type=>"recurrent").first  
      elsif product_name == "AUBURN HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Auburn", :product_type=>"recurrent").first  
      elsif product_name == "BLACK HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Black", :product_type=>"recurrent").first  
      elsif product_name == "FIBER HOLD SPRAY"  
        product = Product.where(:description=>"Hair Illusion Fiber Hold Spray").first  
      elsif product_name == "JET BLACK HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Jet Black", :product_type=>"recurrent").first  
      elsif product_name == "BLONDE HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Blonde", :product_type=>"recurrent").first 
      elsif product_name == "BLONDE HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Blonde", :product_type=>"recurrent").first 
      elsif product_name == "MIRROR"  
        product = Product.where(:description=>"Mirror").first    
      elsif product_name == "LIGHT BROWN HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Light Brown", :product_type=>"recurrent").first  
      elsif product_name == "LIGHT BLONDE HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Light Blonde", :product_type=>"recurrent").first    
      elsif product_name == "DARK BROWN HAIR ILLUSION CLUB"  
        product = Product.where(:description=>"Dark Brown", :product_type=>"recurrent").first 
      elsif product_name == ""
        puts "empty product"
      else 
        puts product_name
      end
      
      if product
        order_item = order.order_items.new(:product_id=>product.id, :quantity=>1, :price=>product.price)
        order_item.save(:validate=>false)
      end
  end

  def self.inside_csv_generation
    logger.info "rake started for generation of csv -#{Time.now}"
  end
  
  
  def self.send_sipwire_rake_started
    logger.info "rake started for -#{Time.now}"
  end
  
  def self.get_total_orders_of_day(order)  
    logger.info "CSVVVVVV call--#{order.created_at}--#{Time.now.inspect}"
    return Order.where("DATE(created_at) =? and eh is not null", order.created_at.to_date).size 
  end

  def self.get_hourly_orders(order,start_hour,end_hour)  
    return Order.where("DATE(created_at) =?",order.created_at.to_date).where("created_at BETWEEN '#{order.created_at.strftime("%Y-%m-%d")} #{start_hour}:00:00.000000' AND '#{order.created_at.strftime("%Y-%m-%d")} #{end_hour}:00:00.000000'") 
  end
  
  def self.InsertToOrdersTable(order, order_items)    
        
    if order["BuyerEmail"]      
      name = order["ShippingAddress"]["Name"].split(" ")
      first_name = name[0] rescue ""
      last_name = name[1] rescue ""
      
      if name.size >2
        last_name = last_name + " " +name[2]
      end
      
      last_name = first_name if last_name.blank?
      
      address1 = ""
      address2 = ""
      city = ""
      state = ""
      zip = ""
      country = ""
      
      billing_address1 = ""
      billing_address2 = ""
      billing_city = ""
      billing_state = ""
      billing_zip = ""
      billing_country = ""
      shipping_carrier = order["ShipmentServiceLevelCategory"] rescue "" 
      
      logger.info "shipping address " 
      puts order["ShippingAddress"].inspect
      
      
      if order["ShippingAddress"]        
        address1 = billing_address1 = order["ShippingAddress"]["AddressLine1"] rescue "" 
        address2 = billing_address2 = order["ShippingAddress"]["AddressLine2"] rescue "" 
        
        address1 = address2 if address1.blank?
        
        city = billing_city = order["ShippingAddress"]["City"] rescue "" 
        state = billing_state = order["ShippingAddress"]["StateOrRegion"] rescue ""   
        zip = billing_zip = order["ShippingAddress"]["PostalCode"] rescue "" 
        country = billing_country = order["ShippingAddress"]["CountryCode"] rescue "" 
        logger.info country.inspect
        
        zip = city if zip.blank?
        state = city if state.blank?
      
      customer = Customer.where(:amazon_customer=>true).where(:email=>order["BuyerEmail"]).first  
 
      unless customer
        customer = Customer.new(:amazon_customer=>true,:email=>order["BuyerEmail"],:first_name=>first_name, :last_name=>last_name, :address1 =>address1,
        :address2=>address2, :city=>city, :state=>state, :zip=>zip, :billing_address1=>billing_address1, :billing_address2=>billing_address2,
        :billing_city=>billing_city, :billing_state=>billing_state, :billing_zip=>billing_zip, :billing_country=>billing_country, :country=>country) 
        puts customer.valid?
        customer.save!
      end 
      
      o=Order.where(:amazon_order_id=>order["AmazonOrderId"]).first
      
      is_shipped = false 
      order_status = "" 
      
      if order["OrderStatus"] == "Shipped"
        is_shipped = true
        order_status = order["OrderStatus"]
      end 
      
      if o
        o.update_attribute(:shipped, is_shipped)     
        o.update_attribute(:order_status, order["OrderStatus"])   
      else 
        o = Order.new(:version_2_order=>true, :order_type=>"normal", :created_at=>order["PurchaseDate"].to_date,:orderer_id=>customer.id, :orderer_type=>"Customer", :host=>order["SalesChannel"], :order_source=>"amazon", :amazon_order_id=>order["AmazonOrderId"], :shipped=>is_shipped, :order_status=>order_status)
        unless o.valid?
          puts o.errors.inspect
        end        
        o.save!

        logger.info "CCCCCCCCCCCCCCCCCCCCc"        
        
        if order_items["OrderItems"]
          
          order_items["OrderItems"].each do |oi|
            logger.info "inside loooo[]"
              logger.info oi.inspect
              logger.info "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
                oi = oi[1]
                logger.info ",,#{oi.class},,,,,,,,,,,,,,,,,,,,,,,#{oi.inspect}"
                if oi.class.to_s == "Hash"
                product_id = 0
                if oi["SellerSKU"] == "UC-XH6U-9EMD"
                  product_id = 2
                elsif oi["SellerSKU"] == "UZ-CAMR-XC8Q"
                  product_id = 19 
                elsif oi["SellerSKU"] == "SPRY"
                  product_id = 35  
                elsif oi["SellerSKU"] == "Q0-WMO7-AS2S"
                  product_id = 6  
                elsif oi["SellerSKU"] == "O1-8ND2-EEOI"
                  product_id = 4 
                elsif oi["SellerSKU"] == "KH-XBRQ-SNMN"
                  product_id = 8 
                elsif oi["SellerSKU"] == "HIFBA18G-LBD"
                  product_id = 17   
                elsif oi["SellerSKU"] == "HIFBA18G-JB"
                  product_id = 10                  
                elsif oi["SellerSKU"] == "HIFBA18G-DB"
                  product_id = 12    
                elsif oi["SellerSKU"] == "HIFBA18G-BR"
                  product_id = 13     
                elsif oi["SellerSKU"] == "HIFBA18G-BL"
                  product_id = 11     
                elsif oi["SellerSKU"] == "HIFBA18G-BD"
                  product_id = 16    
                elsif oi["SellerSKU"] == "G5-MM2U-TNR5"
                  product_id = 3      
                elsif oi["SellerSKU"] == "6P-3DC6-J5UC"
                  product_id = 1  
                  
                elsif oi["SellerSKU"] == "6GBLACK"
                  product_id = 36     
                
                elsif oi["SellerSKU"] == "35-S1QU-XESH"
                  product_id = 7 
                
                elsif oi["SellerSKU"] == "T5-3R2L-Q9G8"
                  product_id = 5          
                
                elsif oi["SellerSKU"] == "0B-1ZMM-55Y3"
                  product_id = 9
                elsif oi["SellerSKU"] == "HN-MPRF-0T1F"
                  product_id = 28 
                end                
                
                price = oi["ItemPrice"]["Amount"].to_f*100 rescue 0
                qty = oi["QuantityOrdered"].to_i rescue 0 
                
                shipping_price = oi["ShippingPrice"]["Amount"].to_f rescue 0
                  
                o.update_attribute(:shipping_price, shipping_price)
                ooo = OrderItem.new(:created_at=>order["PurchaseDate"].to_date , :order_id=>o.id, :product_id=>product_id,:quantity=>qty, :price=>price, :product_type=>"Product")
                ooo.save(:validate=>false)  
              else
                obj = oi[0]                   
                  logger.info "jjjjjjjjjjjjjjjjjjjj---#{obj["SellerSKU"]}"
                  product_id = 0                
                  if obj["SellerSKU"] == "UC-XH6U-9EMD"
                    product_id = 2
                  elsif obj["SellerSKU"] == "UZ-CAMR-XC8Q"
                    product_id = 19 
                  elsif obj["SellerSKU"] == "SPRY"
                    product_id = 35  
                  elsif obj["SellerSKU"] == "Q0-WMO7-AS2S"
                    product_id = 6  
                  elsif obj["SellerSKU"] == "O1-8ND2-EEOI"
                    product_id = 4 
                  elsif obj["SellerSKU"] == "KH-XBRQ-SNMN"
                    product_id = 8 
                  elsif obj["SellerSKU"] == "HIFBA18G-LBD"
                    product_id = 17   
                  elsif obj["SellerSKU"] == "HIFBA18G-JB"
                    product_id = 10                  
                  elsif obj["SellerSKU"] == "HIFBA18G-DB"
                    product_id = 12    
                  elsif obj["SellerSKU"] == "HIFBA18G-BR"
                    product_id = 13     
                  elsif obj["SellerSKU"] == "HIFBA18G-BL"
                    product_id = 11     
                  elsif obj["SellerSKU"] == "HIFBA18G-BD"
                    product_id = 16    
                  elsif obj["SellerSKU"] == "G5-MM2U-TNR5"
                    product_id = 3      
                  elsif obj["SellerSKU"] == "6P-3DC6-J5UC"
                    product_id = 1  
                    
                  elsif obj["SellerSKU"] == "6GBLACK"
                    product_id = 36     
                  
                  elsif obj["SellerSKU"] == "35-S1QU-XESH"
                    product_id = 7 
                  
                  elsif obj["SellerSKU"] == "T5-3R2L-Q9G8"
                    product_id = 5          
                  
                  elsif obj["SellerSKU"] == "0B-1ZMM-55Y3"
                    product_id = 9
                  elsif obj["SellerSKU"] == "HN-MPRF-0T1F"
                    product_id = 28 
                  end
                  
                  price = obj["ItemPrice"]["Amount"].to_f*100 rescue 0
                  qty = obj["QuantityOrdered"].to_i rescue 0
                  
                  shipping_price = obj["ShippingPrice"]["Amount"].to_f rescue 0
                  o.update_attribute(:shipping_price, shipping_price)
                  ooo = OrderItem.new(:created_at=>order["PurchaseDate"].to_date , :order_id=>o.id, :product_id=>product_id,:quantity=>qty, :price=>price, :product_type=>"Product")
                  ooo.save(:validate=>false) 
              end
               
            end

          end
        end
 

      end
    
    end
    
  end
  
  def can_ship_from_amazon
    ship_from_fba = true
    
    if self.orderer && self.orderer.billing_country == "US" && (( self.shipping_code == "Expedited" || self.shipping_code == "Standard" || self.shipping_code == "Priority" ) || !self.ebay_order_id.nil? )
      self.order_items.each do |s|
        ship_from_fba = false if s.amazon_sku.blank? || s.amazon_sku.nil?
      end
    else
      return false
    end
    return ship_from_fba
  end
  
  private
  def shipping_order(order)
    @sub_id ||= 'a' 
    order = Order.new(
      order_id: "%08d" % order.id + "#{@sub_id}",
      created_at: order.created_at,
      orderer: order.orderer
    ) 
    @sub_id = @sub_id.succ 
    return order
  end
end


