class OrderMailer < ActionMailer::Base
  default from: "support@hairillusion.com"

  def receipt(order, total_price,shipping_price)
    @order = order
    @total_price = total_price 
    if shipping_price
       @shipping_cost = shipping_price
    else
       @shipping_cost = order.shipping_cost
    end
    mail(to: @order.orderer.email, subject: 'Hair Illusion Order')
  end
  
  def reset_email(distributor_id)
    @d = Distributor.find distributor_id
    @password = (0...8).map { (65 + rand(26)).chr }.join.downcase
    @d.password = @password
    @d.password_confirmation = @password
    @d.require_password_reset = true  
    @d.save!
    mail(to: @d.email, subject: 'Hairillusion password reset request')    
  end
  
  def charged_notification(customer_id, price)
    @customer = Customer.find customer_id
    @price = price
    mail(to: ['support@hairillusion.com'], bcc: 'kumar234557@gmail.com', subject: "Charged to old hair club customer") 
  end
  
  def error_generated(errors,order_id)
    @errors = errors
    order = Order.find order_id
    mail(to: ['kumar234557@gmail.com'], subject: '#{errors}')    
  end
  
  def order_receipt(order_id) 
    @order = Order.find order_id
    if @order
      @total_price = @order.total_price
      @shipping_cost = @order.get_shipping_cost
    end 
    mail(to: @order.orderer.email, subject: 'Your Hair Illusion Order Details') 
  end
  
  def monthly_order_receipt(order_id) 
    @order = Order.find order_id
    if @order
      @total_price = @order.total_price
      @shipping_cost = @order.get_shipping_cost
    end 
    email_id = @order.orderer.email
    email_id = "freelancer8429@gmail.com" if email_id.blank?
    mail(to: email_id , subject: 'Your Montly Hair Illusion order created') 
  end
  
  def admin_notification(order_id) 
    @order = Order.find order_id
    if @order
      @total_price = @order.total_price
      @shipping_cost = @order.get_shipping_cost
    end 
     
    mail(to: 'support@hairillusion.com', bcc: 'kumar234557@gmail.com', subject: "New order generated from #{@order.host}") 
  end

  def monthly_order_notification(order_id) 
    @order = Order.find order_id
    if @order
      @total_price = @order.total_price
      @shipping_cost = @order.get_shipping_cost
    end 
    mail(to: ['support@hairillusion.com'], bcc: 'kumar234557@gmail.com', subject: "Monthly Automated order generated") 
  end
    
  def send_notification(order, total_price, shipping_price) 
    @order = order
    @total_price = total_price
    if shipping_price
       @shipping_cost = shipping_price
    else
       @shipping_cost = order.shipping_cost
    end
   
    mail(to: ['support@hairillusion.com'], bcc: 'kumar234557@gmail.com', subject: 'Hair Illusion Wholesale Order recieved')
  end

  def shipped(order)
    @order = order
    mail(to: @order.orderer.email, subject: 'Hair Illusion Order Shipped')
  end
   
  def send_order_email(distributor_id, order_id)  
    @order = Order.find order_id
    @distributor = DomainDistributor.where(:id=>distributor_id).first
    emails = "#{@distributor.email},kumar234557@gmail.com" 
    mail(to: emails, subject: "New Order created from your domain-#{@distributor.domain}") 
  end
  
  def send_tracking(order_id)
    @order = Order.find order_id
    @order_items = @order.order_items
    
    @orderer = @order.orderer
    if @order
      @total_price = @order.total_price
      @shipping_cost = @order.get_shipping_cost
    end
    mail(to: @order.orderer.email, :bcc => "freelancer8429@gmail.com", subject: "Your hairillusion order tracking number") 
  end
  
  def send_forum_email(forum_details) 
    @forum_details = forum_details 
    mail(to: ['support@hairillusion.com'], bcc: 'kumar234557@gmail.com', subject: "New forum posted", from:"forum posted")
  end 
  
  def send_shipwire_error(error,order_id) 
    @error = error
    @order_id = order_id
    mail(to: "kumar234557@gmail.com", subject: "shipwire error")    
  end
  
  def send_amazon_error(error,order_id) 
    @error = error
    @order_id = order_id
    mail(to: "kumar234557@gmail.com", subject: "shipwire error")    
  end
   
  def admin_cancel_notification(order_id) 
    @order = Order.find order_id
    @orderer = @order.orderer
    mail(to: ["support@hairillusion.com"], bcc: 'kumar234557@gmail.com', subject: "Order was cancelled") 
  end

  def order_cancel_confirmation(order_id) 
    @order = Order.find order_id 
    @orderer = @order.orderer 
    mail(to: @order.orderer.email, subject: "Order was cancelled") 
  end
  
  def login_details(customer_id)
    @customer = Customer.find customer_id
    mail(to: @customer.email, bcc:'kumar234557@gmail.com', subject: "Hairillusionllc - Action Required") 
  end
 
 def card_updated_notification(customer_id)
    @customer = Customer.find customer_id
    mail(to: 'support@hairillusion.com', bcc: 'kumar234557@gmail.com', subject: "One of the customer updated his card") 
 end
 
 def user_notification(email)
   mail(to: [email], bcc: 'kumar234557@gmail.com', subject: "Please respond on your hairillusion monthly subscription")   
 end
 
 def ebay_notificaton(product_id)
   @product = EbayProduct.find product_id
   mail(to: 'kumar234557@gmail.com', subject: "New Ebay Listing Added") 
 end
 
 def ebay_tracking_error(order_id)
   @order = Order.find order_id
   mail(to: 'kumar234557@gmail.com', subject: "this order tracking updation failed") 
 end
 
 def new_ebay_order(order_id)
   @order = Order.find order_id
   mail(to: 'kumar234557@gmail.com', subject: "New order from ebay") 
 end
    
end