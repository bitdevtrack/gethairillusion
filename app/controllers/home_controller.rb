class HomeController < ApplicationController
  protect_from_forgery :except => [:thankyou]
  require 'shipwire'
  
    after_action :allow_iframe

  def allow_iframe
    response.headers.delete('X-Frame-Options')
  end
      
  def index       
 
    #session[:order_id] = nil
    #session[:card] = nil
    session[:shipping_price] = nil 
    session[:shipping_price] = nil
    session[:country_shipping_code] = nil
    
    session[:cart] = nil
    @orderer = Customer.new
    @order = @orderer.orders.build(order_items_attributes: [quantity: 1])
    @credit_card = CreditCard.new
    @distributor = Distributor.new     
    session[:product_cart] = nil
  end
  
  def clear_all
    session[:product_cart] = nil
    session[:cart] = nil
  end
  
  def customer_login
    
  end
  
  def login
    @error = ""
    if params[:customer][:email].blank? || params[:customer][:password].blank? 
      @error = "Please enter email/password"
    else
      customer = Customer.where(:email=>params[:customer][:email], :password=>params[:customer][:password]).first
      unless customer 
        @error = "Invalid credentials"
      else 
        session[:customer_id] = customer.id 
      end
    end 
  end
  
  def after_payment_page 
    
    if params[:payer_status] == "verified"
      order = Order.find params[:item_number]
      if order 
        order.paid = true         
        order.save 
        customer = Customer.find order.orderer_id
        customer.paypal = params[:payer_email]
        customer.save
        
        OrderMailer.order_receipt(order.id).deliver!  
        OrderMailer.admin_notification(order.id).deliver! 
      end
    end 
  end
  
  def get_shipping
    
    items = []
    address = {}
    @error = ''
    qty = 0
    
    amazon_items = []
    
    if session[:product_cart] && session[:product_cart][:products].size > 0   
      session[:product_cart][:products].each_with_index do |d,index| 
        qty +=d[:quantity].to_i 
        product = Product.where(:description=>d[:name],:product_type=>'normal').first
        if product
          items << { :sku =>  product.sku, :quantity => d[:quantity], :commercialInvoiceValue => d[:price].to_f*d[:quantity].to_f, :commercialInvoiceValueCurrency => 'USD'}
          
          amazon_items << { :SellerSKU =>  product.amazon_sku, :SellerFulfillmentOrderItemId => product.id.to_s, :Quantity => d[:quantity].to_i, :GiftMessage => 'Thank you for ordering hairillusion product'}
        end
      end
       
      if params[:billing_first_name].blank?
         name = params[:first_name] + " " + params[:last_name]
      else
        name = params[:billing_first_name] + " " + params[:billing_last_name]
      end
      
      if params[:billing_address1].blank?
        address1 = params[:address1]
        address2 = params[:address2]
        city = params[:city]
        state = params[:order_state]
        zip = params[:zip]
        country = params[:cb_country]
      else
        address1 = params[:billing_address1]
        address2 = params[:billing_address2]
        city = params[:billing_city]
        state = params[:billing_state]
        zip = params[:billing_zip]
        country = params[:billing_country]
      end
      
      
      if items.size > 0
        address    = { 
          :email    => params[:email],
          :name    => name,
          :company    => "",
          :address1    => address1,
          :address2    => address2,
          :address3    => "",
          :city        => city,
          :state       => state,
          :postalCode  => zip,
          :country    =>  country,
          :phone      => '', 
          :isCommercial    => 0, 
          :isPoBox    => 0
        } 
      end  
      
      if country == "US"  
        
        require "peddler" 
        client = MWS.fulfillment_outbound_shipment(
          primary_marketplace_id: "ATVPDKIKX0DER",
          merchant_id: "A1D4HR9FQLX93E",
          aws_access_key_id: "AKIAJSFSWTEXHLUC4IJQ",
          aws_secret_access_key: "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",
        )  
        amazon_address = {:Name=>name, :Line1=> address1, :Line2=> address2, :City=>city, :StateOrProvinceCode=> state, :PostalCode=>zip, :CountryCode=>'US'}
           
        begin
          result = client.get_fulfillment_preview(amazon_address, amazon_items, opts = {}).parse  
          if result["FulfillmentPreviews"]["member"]
            setup_amazon_carriers(result["FulfillmentPreviews"]["member"])
          else 
            setup_shipwire_carriers(items, address, qty)
          end  
        rescue
          @error = "Please make sure you have entered valid address"
        end
        
        if @carriers.size == 0
          @error = '' 
          begin
            setup_shipwire_carriers(items, address, qty) 
          rescue
            @error = "Please make sure you have entered valid address"
          end
        end
        
      else
        setup_shipwire_carriers(items, address, qty)
      end  
    end 
  end
  
  def set_shipping_values
    shipping_arr = params[:value].split("____")  
    session[:shipping_price] = shipping_arr[0].to_f
    shipping_code = shipping_arr[1]
    session[:country_shipping_code] = shipping_arr[1]  
  end
  
  def faq
  end

  def photos
  end
  
  def forgot_password
    
  end
  
  def forgot_password_email
    @errors = ""
    if params[:email] == ""
      @errors = "Please enter your valid distributor email id"
    else
      distributor = Distributor.where(:email=>params[:email]).first 
      if distributor
        OrderMailer.reset_email(distributor.id).deliver! 
      else
        @errors = "Please enter your valid distributor email id"
      end
    end
  end
  
  def password_reset_success
    
  end
  
  def show_hide_billing_details
    session[:shipping_price] = nil 
    session[:shipping_price] = nil
    session[:country_shipping_code] = nil    
  end
  
  def what_is_it
  end

  def how_it_works
  end

  def about_us
  end

  def color
  end

  def contact_us
  end

  def mens_hair_loss
  end

  def womens_hair_loss
  end

  def thankyou 
  end
  
  def buy_now
    session[:cart] = nil
  end
  
  def after_payment
    redirect_to thank_you_path
  end
  
  def forum 
    @forums = Forum.where(:approved=>true, :domain_name=>"hairillusion.com")#.paginate(per_page: 2, page: params[:page])  
  end
  
  def get_forums
    @forums = []
    if params[:search][:country].blank? && params[:country][:state].blank? 
      @forums = []#.paginate(per_page: 2, page: params[:page])
    elsif params[:search][:country].blank?
      @forums = Forum.where(:domain_name=>request.host,:approved=>true, :state=>params[:country][:state])#.paginate(per_page: 2, page: params[:page])
    elsif params[:country][:state].blank?  
      @forums = Forum.where(:domain_name=>request.host,:approved=>true, :country=>params[:search][:country]).paginate(per_page: 2, page: params[:page])#.paginate(per_page: 2, page: params[:page])
    else
      @forums = Forum.where(:domain_name=>request.host,:approved=>true, :country=>params[:search][:country], :state=>params[:country][:state])#.paginate(per_page: 2, page: params[:page])
    end
  end
  
  def new_forum
    @forum = Forum.new
  end
  
  def get_agent_form
    @agent = Agent.where("name = ?", params[:agent_id]).first   
    @agent = Agent.create(:name=>params[:agent_id]) unless @agent    
    
    session[:cart] = nil
    @orderer = Customer.new
    @order = @orderer.orders.build(order_items_attributes: [quantity: 1])
    @credit_card = CreditCard.new
    @distributor = Distributor.new     
    session[:product_cart] = nil
  end
  
  def save_forum
    @forum = Forum.new(admin_forum_params)
    @forum.state = params[:country][:state]
    @forum.domain_name = request.host
    if @forum.save 
      @forum.update_attribute(:approved,false) 
     # OrderMailer.send_forum_email(@forum.content).deliver!
      @forums = Forum.where(:approved=>true) 
      redirect_to forum_path, notice: 'Your forum request is successfully submitted.'
    else 
      render action: 'new_forum'
    end 
  end
  
  def get_states 
    
    shipping_price = session[:shipping_price] rescue "0" 
    
    cc= CountryPrice.where(:country_code=>params[:parent_region]).last   
    if cc 
      if shipping_price.to_f > 0
        @shipping = shipping_price
      else
        @shipping = session[:shipping_price] = cc.price
      end
      
      if !session[:product_cart].nil? && session[:product_cart][:type] != "recurrent" 
        if session[:product_cart][:products][0][:tax] != @shipping 
          session[:product_cart][:products][0][:tax] = @shipping
        end 
      end
    else
      price_row = ProductPrice.first
      if price_row
        if shipping_price.to_f > 0
          @shipping = shipping_price
        else
          @shipping = session[:shipping_price] = cc.price
        end
      else
        @shipping = 0
      end 
      if !session[:product_cart].nil? && session[:product_cart][:type] != "recurrent" 
        if session[:product_cart][:products][0][:tax] != @shipping 
          session[:product_cart][:products][0][:tax] = @shipping
        end 
      end
    end 
  end
  
  def add_to_cart 
    @url = 'http://hair-illusion-llc.myshopify.com/cart/'
    session[:cart] = Array.new if session[:cart].nil? 
    position = nil
    if session[:cart].size > 0  
      session[:cart].each_with_index do |d,index|
        if d[:name] == params[:product]
          position = index   
        end
      end
      unless position.nil?
        session[:cart][position][:quantity] = session[:cart][position][:quantity].to_i + params[:quantity].to_i
        if params[:product_type] == "upsell"
          session[:cart][position][:price] = session[:cart][position][:price].to_f + (19.95 * params[:quantity].to_i)
        else
          session[:cart][position][:price] = session[:cart][position][:price].to_f + (params[:unit_price].to_f * params[:quantity].to_i)
        end 
      else
        if params[:product_type] == "upsell"
        h = {:product_type =>params[:product_type], :name => params[:product], :product_id => params[:upsell_id], :quantity => params[:quantity], :price=> (19.95*params[:upsell_qty].to_i)}           
        else
        h = {:name => params[:product], :product_id => params[:product_id], :quantity => params[:quantity], :price=> (params[:unit_price].to_f*params[:quantity].to_i)}           
        end
        session[:cart] << h
      end
    else
      if params[:product_type] == "upsell"
        h = {:product_type =>params[:product_type], :name => params[:product], :product_id => params[:upsell_id], :quantity => params[:quantity], :price=> (19.95*params[:upsell_qty].to_i)}           
      else
        h = {:name => params[:product], :product_id => params[:product_id], :quantity => params[:quantity], :price=> (params[:unit_price].to_f*params[:quantity].to_i)}           
      end
      session[:cart] << h
    end
    str = ""
    session[:cart].each do |d|
      str = str + "," if str.length > 0
      str = str + d[:product_id].to_s + ":"+d[:quantity].to_s
    end
    @url = @url + str + "?source_app=shopify-widget?referer="+params[:referel]
  end
  
  def remove_from_cart
    @url = 'http://hair-illusion-llc.myshopify.com/cart/'
    if session[:cart].size > 0
      session[:cart] = session[:cart].reject { |h| h[:name] == params[:product]  } 
    end
    
    if session[:cart].size > 0
      str = ""
      session[:cart].each do |d|
        str = str + "," if str.length > 0
        str = str + d[:product_id].to_s + ":"+d[:quantity].to_s
      end
      @url = @url + str + "?source_app=shopify-widget?referer="+params[:referel]
    end
  end
  
  def add_product_to_cart
    product_type = params[:product_type]
    if session[:product_cart].nil? 
      
      session[:product_cart] = Hash.new
      session[:product_cart][:type] = params[:type] 
       
      if params[:type].to_s == "recurrent"  
        session[:product_cart][:name] = params[:val] #+ " - HAIR ILLUSION – FREE SHIPPING + BONUS HAIRLINE OPTIMIZER"
        session[:product_cart][:price] = 0
        session[:product_cart][:tax] = 8.95
        session[:product_cart][:total] = 8.95 
      else 
        session[:product_cart][:products] = []
        if params[:jet_black].to_i > 0 
          if product_type == "one_time_small"
            h = {:name => "Jet Black 18g", :quantity => params[:jet_black].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Jet Black", :quantity => params[:jet_black].to_i, :price=> @price, :tax => @shipping }     
          end
          session[:product_cart][:products] << h
        end
        
        if params[:black].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Black 18g", :quantity => params[:black].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Black", :quantity => params[:black].to_i, :price=> @price, :tax => @shipping }     
          end                    
          session[:product_cart][:products] << h
        end
        
        if params[:dark_brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Dark Brown 18g", :quantity => params[:dark_brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Dark Brown", :quantity => params[:dark_brown].to_i, :price=> @price, :tax => @shipping }     
          end          
          session[:product_cart][:products] << h
        end
        
        if params[:brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Brown 18g", :quantity => params[:brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Brown", :quantity => params[:brown].to_i, :price=> @price, :tax => @shipping }     
          end        
          session[:product_cart][:products] << h
        end
        
        if params[:light_brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Light Brown 18g", :quantity => params[:light_brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Light Brown", :quantity => params[:light_brown].to_i, :price=> @price, :tax => @shipping }     
          end           
          session[:product_cart][:products] << h
        end
        
        if params[:aubrown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Auburn 18g", :quantity => params[:aubrown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Auburn", :quantity => params[:aubrown].to_i, :price=> @price, :tax => @shipping }     
          end             
          session[:product_cart][:products] << h
        end
        
        if params[:blonde].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Blonde 18g", :quantity => params[:blonde].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Blonde", :quantity => params[:blonde].to_i, :price=> @price, :tax => @shipping }     
          end           
          session[:product_cart][:products] << h
        end
        
        if params[:light_blonde].to_i > 0 
          if product_type == "one_time_small"
            h = {:name => "Light Blonde 18g", :quantity => params[:light_blonde].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Light Blonde", :quantity => params[:light_blonde].to_i, :price=> @price, :tax => @shipping }     
          end 
          session[:product_cart][:products] << h
        end 
      end
    else 
      session[:product_cart][:type] = params[:type]
      if params[:type] == "recurrent"
        session[:product_cart][:product_type] = params[:type]
        session[:product_cart][:name] = params[:val] #+ " - HAIR ILLUSION – FREE SHIPPING + BONUS HAIRLINE OPTIMIZER"
        session[:product_cart][:product_name] = params[:val]
        session[:product_cart][:price] = 0
        session[:product_cart][:tax] = 8.95
        session[:product_cart][:total] = 8.95
      else 
        session[:product_cart][:products] = []
        
        if params[:jet_black].to_i > 0 
          if product_type == "one_time_small"
            h = {:name => "Jet Black 18g", :quantity => params[:jet_black].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Jet Black", :quantity => params[:jet_black].to_i, :price=> @price, :tax => @shipping }     
          end
          session[:product_cart][:products] << h
        end
        
        if params[:black].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Black 18g", :quantity => params[:black].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Black", :quantity => params[:black].to_i, :price=> @price, :tax => @shipping }     
          end                    
          session[:product_cart][:products] << h
        end
        
        if params[:dark_brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Dark Brown 18g", :quantity => params[:dark_brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Dark Brown", :quantity => params[:dark_brown].to_i, :price=> @price, :tax => @shipping }     
          end          
          session[:product_cart][:products] << h
        end
        
        if params[:brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Brown 18g", :quantity => params[:brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Brown", :quantity => params[:brown].to_i, :price=> @price, :tax => @shipping }     
          end        
          session[:product_cart][:products] << h
        end
        
        if params[:light_brown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Light Brown 18g", :quantity => params[:light_brown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Light Brown", :quantity => params[:light_brown].to_i, :price=> @price, :tax => @shipping }     
          end           
          session[:product_cart][:products] << h
        end
        
        if params[:aubrown].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Auburn 18g", :quantity => params[:aubrown].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Auburn", :quantity => params[:aubrown].to_i, :price=> @price, :tax => @shipping }     
          end             
          session[:product_cart][:products] << h
        end
        
        if params[:blonde].to_i > 0
          if product_type == "one_time_small"
            h = {:name => "Blonde 18g", :quantity => params[:blonde].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Blonde", :quantity => params[:blonde].to_i, :price=> @price, :tax => @shipping }     
          end           
          session[:product_cart][:products] << h
        end
        
        if params[:light_blonde].to_i > 0 
          if product_type == "one_time_small"
            h = {:name => "Light Blonde 18g", :quantity => params[:light_blonde].to_i, :price=> @small_price, :tax => @shipping }           
          else
            h = {:name => "Light Blonde", :quantity => params[:light_blonde].to_i, :price=> @price, :tax => @shipping }     
          end 
          session[:product_cart][:products] << h
        end         
      end   
      
      if session[:product_cart][:products].empty? 
        session[:product_cart] = nil
      end
    end    
  end
  
  def portal
    session[:cart] = nil
    @orderer = Customer.new
    @order = @orderer.orders.build(order_items_attributes: [quantity: 1])
    @credit_card = CreditCard.new
    @distributor = Distributor.new     
    session[:product_cart] = nil
  end
  
  def create_portal_order
    @order = Order.new
    redirect_to '/portal'
  end
 
 private  
  def admin_forum_params
    params.require(:forum).permit(:subject, :content, :approved, :country, :state, :address, :name)
  end 
  
  def setup_shipwire_carriers(items,address, qty)
          
    payload = {options: {  currency: "USD", groupBy: "all" }, order: { shipTo: address, items: items }}  
    Shipwire.configure do |config|
      config.username = "support@hairillusion.com"
      config.password = "ZZZack!!!"
      config.endpoint = URI::encode('https://api.shipwire.com')
    end 
    response = Shipwire::Rate.new.find(payload)  
         
    begin 
      if response.body['error_summary'].blank? 
        options = response.body['resource']['rates'][0]['serviceOptions'] rescue [] 
        @carriers = []
        options.each do |rate|
          obj = rate['shipments'][0]   
          amount = obj['cost']['amount'].to_f
          handling = 1.95 + (qty-1)*0.7
          amount = amount + handling 
          amount = sprintf('%.2f', amount) 
          
          carrier = [obj['carrier']['description'].downcase!]
          logger.info ".......--#{carrier}"
          if carrier.include? "usps first-class mail international"
            #@carriers << { shipping_code: obj['carrier']['code'], name: obj['carrier']['description'], deliver_min_date: obj['expectedDeliveryMinDate'].to_date+1.day,deliver_max_date: obj['expectedDeliveryMaxDate'].to_date+2.day, amount: amount, currency: obj['cost']['currency'] } 
          else
            @carriers << { shipping_code: obj['carrier']['code'], name: obj['carrier']['description'], deliver_min_date: obj['expectedDeliveryMinDate'].to_date+1.day,deliver_max_date: obj['expectedDeliveryMaxDate'].to_date+2.day, amount: amount, currency: obj['cost']['currency'] }     
          end

        end  
      else
        @error = response.body['error_summary']
      end  

    rescue
      @error = "Please enter valid address"
    end 
    
    logger.info @carriers.inspect
    
  end
  
  def setup_amazon_carriers(carriers)
    begin
    @carriers = []
    carriers.each do |carrier|  
      @carriers << { shipping_code: carrier["ShippingSpeedCategory"], name: carrier["ShippingSpeedCategory"], deliver_min_date: carrier['FulfillmentPreviewShipments']['member']['LatestArrivalDate'].to_date, deliver_max_date: carrier['FulfillmentPreviewShipments']['member']['LatestArrivalDate'].to_date+3.days, amount: get_amount_from_hash(carrier['EstimatedFees']['member']), currency: 'S' } 

    end   
    rescue
      @error = "Please make sure you have entered valid address"
    end
  end
  
  def get_amount_from_hash(arr) 
    
    amount = 0 
    arr.each do |s|
      amount += s['Amount']['Value'].to_f
    end 
    return amount
  end
end
