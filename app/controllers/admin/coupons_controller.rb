class Admin::CouponsController < Admin::ApplicationController 
  
  def index
    @coupons = Coupon.all
  end
  
  def new
    @coupon = Coupon.new
  end
  
  def create
    @coupon = Coupon.new(coupons_params) 
    if @coupon.save
      redirect_to '/admin/coupons', notice: 'Coupon was successfully created.'
    else
      render action: 'new'
    end
  end
  
  def edit
    @coupon = Coupon.find params[:id]
  end
  
  def update 
    @coupon = Coupon.find(params[:id])  
    if @coupon.update_attributes(coupons_params)
      redirect_to '/admin/coupons', notice: 'Coupon was successfully updated.'
    else
      render :edit
    end 
  end
  
  def destroy
    @coupon = Coupon.find params[:id]
    if @coupon
      @coupon.destroy
      redirect_to '/admin/coupons', notice: 'Coupon was successfully destroyed.'
    else
      redirect_to '/admin/coupons'
    end 
  end
  
  def logout
    reset_session
    redirect_to '/'
  end

private

  def coupons_params
    params.require(:coupon).permit(:name,:discount_type, :discount_value, :active)
  end
    
end
