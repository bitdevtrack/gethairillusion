class Admin::SubscribersController < Admin::ApplicationController
  
  def index 
    if params && params[:search]
      unless params[:search][:start_date].blank? 
        @subscribers = Subscriber.where(:active=>true).where("created_at >= ?", Date.strptime(params[:search][:start_date], "%m/%d/%Y"))
      end 
       unless params[:search][:end_date].blank? 
        @subscribers = @subscribers.where(:active=>true).where("created_at <= ?", Date.strptime(params[:search][:end_date], "%m/%d/%Y"))
      end  
    else
      @subscribers = Subscriber.where(:active=>true)  
    end
    @subscribers = @subscribers.order(" created_at desc")
  end  
  
  def download_contacts_csv
    @subscribers = []
    
    unless params[:from_date].blank? 
        @subscribers = Subscriber.where(:active=>true).where("created_at >= ?", Date.strptime(params[:from_date], "%m/%d/%Y"))
      end 
       unless params[:to_date].blank? 
        @subscribers = @subscribers.where(:active=>true).where("created_at <= ?", Date.strptime(params[:to_date], "%m/%d/%Y"))
      end  
      if params[:from_date].blank? && params[:from_date].blank? 
        @subscribers = Subscriber.where(:active=>true) 
      end 

   csv_string = CSV.generate(:force_quotes => true) do |csv|
      csv<<["emails"]
      @subscribers.each do |s|
        csv << [s.email]
      end 
    end
    file_name= "public/subscribers.csv"
    File.open(file_name, "w") { |f| f << csv_string }    
    send_file file_name, :type => 'text/csv; charset=iso-8859-1; header=present', :disposition => "attachment; filename=subscribers.csv" 
 
  end
  
  def destroy
    s = Subscriber.find params[:id]
    s.update_attribute(:active, false)
    s.save
    
    redirect_to admin_subscribers_path, :notice=>"Successfully removed from subscribers list."
  end 

end
