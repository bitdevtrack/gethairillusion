class Admin::CountryPricesController < Admin::ApplicationController  
  
  def index
    @country_prices = CountryPrice.all
  end
  
  def update_price
    if params[:country_prices].present?
      params[:country_prices].each do |c|
        logger.info c.inspect
        cc = CountryPrice.find_by_country c["name"]
        cc.update_attribute(:price, c["price"].to_f) if cc
      end
    end
      
    redirect_to admin_country_prices_path
  end
  
end
