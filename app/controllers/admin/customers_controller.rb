class Admin::CustomersController < Admin::ApplicationController
  respond_to :html
  respond_to :js, only: [:index]

  def index
    respond_to do |format|
      format.html {}
      format.js do
        @customers = Customer.all

        grid_table_for(@customers, index_params)
      end
    end
  end

  def show
    @customer = Customer.find(params[:id])
  end

  def edit
    @customer = Customer.find(params[:id])
  end
  
  def update
    @customer = Customer.find(params[:id]) 

    if @customer.update_attributes(customer_params)
      redirect_to admin_customer_path(@customer)
    else
      render :edit
    end
  end

  private
    def index_params
      params.permit [].concat(Customer.grid_table_strong_params)
    end
    
    def customer_params
      params.require(:customer).permit(:first_name,:last_name, :email, :address1, :address2,
          :city, :country, :state, :zip, :billing_first_name, :billing_last_name, :billing_email, :billing_address1, :billing_address2, :billing_city, :billing_state, :billing_zip, :billing_country)
    end
end
