class Admin::OrdersController < Admin::ApplicationController
  respond_to :html, except: [:shipping]
  respond_to :js, only: [:index]
    
  
  def distributor_orders
    distributor_ids = Distributor.all.collect(&:id) 
    @orders = Order.where("orderer_id in (?) and orderer_type='Distributor'",distributor_ids).order("DATE(created_at) DESC").page(params[:page]).per_page(10) 
  end
  
  def get_refund
    @orders = []
  end

  def release_order
    @order = Order.find(params[:id]) 
    @order.held = false
    @order.save 
    redirect_to :back
  end
   
  def send_invoice
    @order = Order.find(params[:id]) 
    OrderMailer.order_receipt(@order.id).deliver! 
    redirect_to :back, :notice=>"Invoice sent to customer"
  end
  
  def get_refundable_orders
    first_name = (params[:search][:first_name].present? && params[:search][:first_name]!='') ? params[:search][:first_name].to_s : params[:search][:first_name]
    last_name = (params[:search][:last_name].present? && params[:search][:last_name]!='') ? params[:search][:last_name].to_s : params[:search][:last_name]
    
    email = (params[:search][:email].present? && params[:search][:email]!='') ? params[:search][:email].to_s : params[:search][:email]
 

    query = "select o.id as order_id, o.refunded_at as refunded_at, o.shopify_order_id as shopify_order_id, o.created_at as created_at,c.first_name as first_name, c.last_name as last_name, c.email as email_id, o.last_delivery_date as last_date from orders o 
          left join customers c on o.orderer_id = c.id where "
          
          query += " c.first_name LIKE '%#{first_name}%' and " unless first_name.blank? 
          query += " c.last_name LIKE '%#{last_name}%' and " unless last_name.blank? 
          query += " c.email LIKE '%#{email}%' and " unless email.blank?
          
          query +=" o.cancelled=false and ( o.stripe_id is not null or shopify_order_id is not null) and refunded_at is null order by created_at desc"


          @orders = Order.find_by_sql query    
  end
   
  def club_orders
    @orders = []
    @total_active_orders = Order.where("order_type = 'recurrent' and cancelled = false").size
  end
  
  def change_date
    @return_url = request.referer
    @order = Order.find params[:id]
  end
  
  def update_customer_price
    customer = Customer.find params[:customer_id]
    @error = ""
    if customer.update_attribute(:price, params[:price].to_f)
       @error = "saved"  
    end 
  end
  
  def update_date
    @order = Order.find params[:order_id]
    @error = ""
    if @order.update_attribute(:next_delivery_date, params[:order][:next_delivery_date])
       @error = "saved" 
    end 
    
  end
  
  def cancel_orders
    @orders = []
  end
  
  def update_days_gap
    @order = Order.find params[:order_id]
    @error = ""
    if @order.update_attribute(:gap_days, params[:order][:gap_days])
       @error = "saved" 
    end 
    if @order.last_delivery_date
      @order.next_delivery_date = @order.last_delivery_date + params[:order][:gap_days].to_i.days
    else
      @order.next_delivery_date = Date.today + params[:order][:gap_days].to_i.days
    end 
    @order.save
  end
  
  def get_orders
    first_name = (params[:search][:first_name].present? && params[:search][:first_name]!='') ? params[:search][:first_name].to_s : params[:search][:first_name]
    last_name = (params[:search][:last_name].present? && params[:search][:last_name]!='') ? params[:search][:last_name].to_s : params[:search][:last_name]
    
    email = (params[:search][:email].present? && params[:search][:email]!='') ? params[:search][:email].to_s : params[:search][:email]
    
    
    query = "select o.id as order_id, o.created_at as created_at,c.first_name as first_name, c.last_name as last_name, c.email as email_id, o.last_delivery_date as last_date from orders o 
          left join customers c on o.orderer_id = c.id where "
          
          query += " c.first_name LIKE '%#{first_name}%' and " unless first_name.blank? 
          query += " c.last_name LIKE '%#{last_name}%' and " unless last_name.blank? 
          query += " c.email LIKE '%#{email}%' and " unless email.blank?
          
          query +=" (o.cancelled=false and parent_order_id is null and order_type='recurrent')  order by o.id desc"

          @orders = Order.find_by_sql query
  end 
  
  def filter_orders
    @total_active_orders = Order.where("order_type = 'recurrent' and cancelled = false").size
    @orders = []
    orders = Order.where("version_2_order = true and order_type = 'recurrent' and parent_order_id is null") 
    
    first_name = (params[:search][:first_name].present? && params[:search][:first_name]!='') ? params[:search][:first_name].to_s : params[:search][:first_name]
    last_name = (params[:search][:last_name].present? && params[:search][:last_name]!='') ? params[:search][:last_name].to_s : params[:search][:last_name]
    
    email = (params[:search][:email].present? && params[:search][:email]!='') ? params[:search][:email].to_s : params[:search][:email]
    
    
          query = "select o.* from orders o 
          left join customers c on o.orderer_id = c.id where " 
          query += " c.first_name LIKE '%#{first_name}%' and " unless first_name.blank? 
          query += " c.last_name LIKE '%#{last_name}%' and " unless last_name.blank? 
          query += " c.email LIKE '%#{email}%' and " unless email.blank?

          query +=" (o.cancelled=false and o.parent_order_id is null and o.order_type='recurrent')" 
 
          @orders = Order.find_by_sql query     
  end
  
  def index
    if params[:customer_id]
      @orderer = Customer.find(params[:customer_id])
    elsif params[:distributor_id]
      @orderer = Distributor.find(params[:distributor_id])
    end

    respond_to do |format|
      format.html {}
      format.js do
        if @orderer.present?
          @orders = @orderer.orders.where(:cancelled=>false, :paid=>true)
        else
          @orders = Order.where(:cancelled=>false, :paid=>true).includes(:orderer)
        end  
        @orders = @orders.order("id desc")
        grid_table_for(@orders, index_params)
      end
    end
  end

  def show
    @order = Order.find(params[:id])
  end
  
  def show_price
    
  end
  
  def cancel_order
    order = Order.find params[:order_id]
    if order
      order.cancelled = true
      order.cancelled_at = Time.now
      if order.save
        OrderMailer.order_cancel_confirmation(order.id).deliver! 
        OrderMailer.admin_cancel_notification(order.id).deliver! 
      end
    end 
  end

  def cancel1_order
    order = Order.find params[:order_id]
    if order
      order.cancelled = true
      order.cancelled_at = Time.now
      if order.save 
      end
    end 
  end

  def cancel_order2
    order = Order.find params[:order_id]
    if order
      order.cancelled = true
      order.cancelled_at = Time.now
      if order.save
        OrderMailer.order_cancel_confirmation(order.id).deliver! 
        OrderMailer.admin_cancel_notification(order.id).deliver! 
      end
    end 
  end
  
  def update_price
    
    price = ProductPrice.first
    if price
      price.update_attributes(:price=>params[:price].to_f, :recurrent_price=>params[:recurrent_price].to_f, :small_product_price=>params[:small_product_price].to_f, :shipping_price => params[:shipping_price].to_f, :combo2_price=>params[:combo2_price].to_f, :combo3_price=>params[:combo3_price].to_f)
    else
      ProductPrice.create(:price=>params[:price].to_f, :recurrent_price=>params[:recurrent_price].to_f, :small_product_price=>params[:small_product_price].to_f, :shipping_price => params[:shipping_price].to_f, :combo2_price=>params[:combo2_price].to_f, :combo3_price=>params[:combo3_price].to_f)
    end
    
    if(params[:price].to_f>0)
      products = Product.where(:product_type=>"normal").where("description in (?)", ['Jet Black','Black','Dark Brown','Brown','Light Brown','Auburn','Blonde', 'Light Blonde'])
      products.each do |p| 
        p.update_attribute(:price, params[:price].to_f*100)
      end
    end
    
    if(params[:small_product_price].to_f>0)
      products = Product.where(:product_type=>"normal").where("description in (?)", ['Jet Black 18g','Black 18g','Dark Brown 18g','Brown 18g','Light Brown 18g','Auburn 18g','Blonde 18g', 'Light Blonde 18g'])
      products.each do |p| 
        p.update_attribute(:price, params[:small_product_price].to_f*100)
      end
    end
    
    if(params[:valuepack2_price].to_f>0)
      product = Product.where(:description=>"Value Pack 2").first
      product.update_attribute(:price, params[:valuepack2_price].to_f*100) if product
    end
    
    if(params[:spray_combo_price].to_f>0) 
      ProductPrice.first.update_attribute(:spray_combo_price, params[:spray_combo_price].to_f*100)
    end 
    
    if(params[:valuepack2_shipping_price].to_f>0)
      product = Product.where(:description=>"Value Pack 2").first
      product.update_attribute(:shipping_price, params[:valuepack2_shipping_price].to_f*100) if product
    end
    
    if(params[:valuepack3_shipping_price].to_f>0)
      product = Product.where(:description=>"Value Pack 3").first
      product.update_attribute(:shipping_price, params[:valuepack3_shipping_price].to_f*100) if product
    end
    
    if(params[:valuepack4_shipping_price].to_f>0)
      product = Product.where(:description=>"Value Pack 4").first
      product.update_attribute(:shipping_price, params[:valuepack4_shipping_price].to_f*100) if product
    end
    
    if(params[:valuepack5_shipping_price].to_f>0)
      product = Product.where(:description=>"Value Pack 5").first
      product.update_attribute(:shipping_price, params[:valuepack5_shipping_price].to_f*100) if product
    end
    
    if(params[:combo_shipping_price].to_f>0)
      product = Product.where(:description=>"Combo Pack").first
      product.update_attribute(:shipping_price, params[:combo_shipping_price].to_f*100) if product
    end 
    
    if(params[:valuepack3_price].to_f>0)
      product = Product.where(:description=>"Value Pack 3").first
      product.update_attribute(:price, params[:valuepack3_price].to_f*100) if product
    end
    
    if(params[:valuepack4_price].to_f>0)
      product = Product.where(:description=>"Value Pack 4").first
      product.update_attribute(:price, params[:valuepack4_price].to_f*100) if product
    end
    
    if(params[:valuepack5_price].to_f>0)
      product = Product.where(:description=>"Value Pack 5").first
      product.update_attribute(:price, params[:valuepack5_price].to_f*100) if product
    end
    
    if(params[:combo_price].to_f>0)
      product = Product.where(:description=>"Combo Pack").first
      product.update_attribute(:price, params[:combo_price].to_f*100) if product
    end  
    
    logger.info params.inspect
    
    if(params[:spray_price].to_f>0)
      product = Product.where(:description=>"Hair Illusion Fiber Hold Spray").first
      product.update_attribute(:price, params[:spray_price].to_f*100) if product
    end  
    
    if(params[:water_price].to_f>0)
      product = Product.where(:description=>"Water Resistant Spray").first
      product.update_attribute(:price, params[:water_price].to_f*100) if product
    end 
    
    if(params[:applicator_price].to_f>0)
      product = Product.where(:description=>"Spray Applicator").first
      product.update_attribute(:price, params[:applicator_price].to_f*100) if product
    end 
    
    if(params[:black6g_price].to_f>0)
      product = Product.where(:description=>"Black 6g").first
      product.update_attribute(:price, params[:black6g_price].to_f*100) if product
    end 
     
    if(params[:laser_comb_price].to_f>0)
      product = Product.where(:description=>"Laser Comb").first
      product.update_attribute(:price, params[:laser_comb_price].to_f*100) if product
    end 
    
    if(params[:mirror_price].to_f>0)
      product = Product.where(:description=>"Mirror").first
      product.update_attribute(:price, params[:mirror_price].to_f*100) if product
    end 
    
    if(params[:opt_price].to_f>0)
      product = Product.where(:description=>"Optimizer").first
      product.update_attribute(:price, params[:opt_price].to_f*100) if product
    end 
    
    if params[:laser_comb_price].to_f > 0
      product = Product.where(:description=>"Laser Comb").first
      product.update_attribute(:price, params[:laser_comb_price].to_f*100) if product
    end
    
  end

  def new
    if params[:distributor_id]
      @distributor = Distributor.find(params[:distributor_id])
      @order = @distributor.orders.build
      @credit_card = CreditCard.new

      Product.where("product_type != 'recurrent'").each do |p|
        @order.order_items<< OrderItem.new(product: p, quantity: 0)
      end
    elsif params[:customer_id]
      @customer = Customer.find(params[:customer_id])
      @order = @customer.orders.build
      @order_item = @order.order_items.build
      @credit_card = CreditCard.new
    else
      @customer = Customer.new
      @order = @customer.orders.build
      @order_item = @order.order_items.build
      @credit_card = CreditCard.new
    end
  end

  def create
    if params[:distributor_id].present?
      @orderer = Distributor.find(params[:distributor_id])
      orderer_valid = true
    elsif params[:customer_id].present?
      @orderer = Customer.find(params[:customer_id])
      orderer_valid = true
    else
      @orderer = Customer.new(customer_params)
      orderer_valid = @orderer.valid?
    end

    @order = @orderer.orders.build(order_params)
    @order.host = request.host
    @credit_card = CreditCard.new(cc_params)

    order_valid = @order.valid?
    credit_card_valid = @credit_card.valid?

    if orderer_valid && order_valid && credit_card_valid
      begin
        if @orderer.stripe_id.blank?
          stripe_customer = Stripe::Customer.create(
            email: @orderer.email
          )

          @orderer.stripe_id = stripe_customer.id
        end

        total_price = @order.total(@orderer.try(:price))

        charge = Stripe::Charge.create(
          amount: total_price,
          description: @order.description,
          currency: 'usd',
          card: cc_params
        )

        @order.stripe_id = charge.id 
        @orderer.save!
        @order.save! 
        OrderMailer.receipt(@order, total_price, nil).deliver

        flash[:notice] = "Thanks for your order"
        redirect_to params[:distributor_id].present? ? admin_distributor_path(@orderer) : admin_customer_path(@orderer)
      rescue Stripe::CardError => e
        # Since it's a decline, Stripe::CardError will be caught
        body = e.json_body
        err  = body[:error]

        puts "Status is: #{e.http_status}"
        puts "Type is: #{err[:type]}"
        puts "Code is: #{err[:code]}"
        # param is '' in this case
        puts "Param is: #{err[:param]}"
        puts "Message is: #{err[:message]}"

        flash[:alert] = err[:message]

        render admin_order_path(@order)
      rescue => e
        # Something else happened, completely unrelated to Stripe
        flash[:alert] = e.message
        render admin_order_path(@order)
      end
    else
      render admin_order_path(@order)
    end
  end

  def edit
    @order = Order.find(params[:id])
    if @order.orderer_type == Distributor.name
      @distributor = @order.orderer
    elsif @order.orderer_type == Customer.name
      @customer = @order.orderer
      @order_item = @order.order_items.build
    end

    @credit_card = CreditCard.new
  end

  def update
    @order = Order.find(params[:id])

    if @order.update_attributes(order_params)
      redirect_to admin_order_path(@order)
    else
      render :edit
    end
  end

  def refund
    @error = '' 
    @order = Order.find(params[:id])  
    
    if @order.shopify_order_id 
        shop_url = "https://a98b179d72117d149e44ae83796e4c64:b62b5da657f75898e1d45eb6a6e0e247@hair-illusion-llc.myshopify.com/admin" 
        ids = [] 
        gateway = ""
        #fetch transactions
        refund_url = shop_url+"/orders/#{@order.shopify_order_id}/transactions.json" 
        result = HTTParty.get refund_url  
        result["transactions"].each do |s|  
          ids << s["id"]
          gateway = s["gateway"] 
        end  
        
        if ids.empty?
          @error = "Couldnt find shopify payments"
        else 
          shop_url = shop_url+"/orders/#{@order.shopify_order_id}/refunds.json"  
            refund = HTTParty.get refund_url  
            query_params = Hash.new  
            line_items = []
            ShopifyAPI::Base.site = shop_url 
            order = ShopifyAPI::Order.find(@order.shopify_order_id )   
            order.line_items.each do |item|   
              line_items << { "line_item_id" => item.id, "quantity" => item.quantity}
            end 
            query_params["refund"] =  { "restock" => true, "notify" => true,"note" => "refund requested", "shipping"=> {"full_refund" => true}  }
            query_params["refund"]["transactions"] = [{ "parent_id"=> result["transactions"][0]["id"], "amount"=> result["transactions"][0]["amount"].to_f, "kind"=> "refund", "gateway"=> gateway }]
            query_params["refund"]["refund_line_items"] = line_items
  
            begin
              refund = HTTParty.post(shop_url, :query => query_params)   
              unless refund.parsed_response["errors"].present? 
                order.cancel(email: false, restock: true)  
                @order.update_attributes(:refunded_at => Time.now, :cancelled=>true, :cancelled_at=>Time.now)
                @error = "Successfully refunded to customer" 
              else
                OrderMailer.send_shipwire_error(refund,@order.id).deliver!
                @error = "#{refund}" 
              end 
            rescue => e
              # Something else happened, completely unrelated to Stripe
              @error = e.message 
            end   
        end  
    else
      begin
        @order.stripe_charge.refund
        #@order.update_attribute(:refunded_at, Time.now)
        @order.update_attributes(:refunded_at => Time.now, :cancelled=>true, :cancelled_at=>Time.now)
        @error = "Successfully refunded to customer" 
      rescue => e
        # Something else happened, completely unrelated to Stripe
        @error = e.message 
      end
    end
    
  end
  
  def pull_details_from_shopify
    shop_url = "https://a98b179d72117d149e44ae83796e4c64:b62b5da657f75898e1d45eb6a6e0e247@hair-illusion-llc.myshopify.com/admin"
    ShopifyAPI::Base.site = shop_url 
    
    customers = ShopifyAPI::Customer.all  
    i = 0
    customers.each do |c|  
      email = c.email
      db_customer = Customer.find_by_email(email) 
       
      if db_customer.nil?
        address = c.default_address
        i = i+1
        customer = Customer.new(:email=>email, :first_name=>c.first_name, :last_name=>c.last_name, :address1 => address.address1,:address2 => address.address2, :created_at=>c.created_at,
        :city=> address.city, :country =>address.country_code, :state=>address.province, :zip=>address.zip, :stripe_id=>"shopify"+Time.now.to_i.to_s+i.to_s, :shopify_customer_id=>c.id)
        customer.save(validate: false)    
      else 
        db_customer.shopify_customer_id = c.id
        db_customer.save(validate: false)
      end
    end
    
    orders = ShopifyAPI::Order.all 
    orders.each do |o|   
      order = Order.find_by_shopify_order_id(o.id)
      if order.nil? 
        customer = Customer.find_by_shopify_customer_id(o.customer.id) 
        customer_id = customer.id 
        
        order = Order.new(:orderer_id=>customer_id, :orderer_type=>'Customer', :shopify_order_id=>o.id, :created_at=>o.processed_at)
        order.save
        
        host = ""
        landing_site = o.landing_site
         
        if landing_site.include?('hairillusion.net')
          host = "hairillusion.net" 
          distributor = DomainDistributor.where(:domain=>"hairillusion.net").first
          if distributor
            distributor_order = DistributorOrder.new(:distributor_id=>distributor.id, :order_id=>order.id, :created_at=>order.created_at)
            distributor_order.save
          end
        elsif landing_site.include?('buyhairillusion.com') 
          host = "buyhairillusion.com" 
          distributor = DomainDistributor.where(:domain=>"buyhairillusion.com").first
          if distributor
            distributor_order = DistributorOrder.new(:distributor_id=>distributor.id, :order_id=>order.id, :created_at=>order.created_at)
            distributor_order.save
          end
        elsif landing_site.include?('gethairillusion.com') 
          host = 'gethairillusion.com'
          distributor = DomainDistributor.where(:domain=>"gethairillusion.com").first
          if distributor
            distributor_order = DistributorOrder.new(:distributor_id=>distributor.id, :order_id=>order.id, :created_at=>order.created_at)
            distributor_order.save
          end 
        elsif landing_site.include?('hairillusion.com') 
          host = 'hairillusion.com'
          distributor = DomainDistributor.where(:domain=>"hairillusion.com").first
          if distributor
            distributor_order = DistributorOrder.new(:distributor_id=>distributor.id, :order_id=>order.id, :created_at=>order.created_at)
            distributor_order.save
          end 
        else  
          DomainDistributor.all.each do |d| 
            if landing_site.include?(d.domain)  
              distributor_order = DistributorOrder.new(:distributor_id=>d.id, :order_id=>order.id, :created_at=>order.created_at)
              distributor_order.save
              host = "#{d.domain}"  
              OrderMailer.send_order_email(distributor_order.distributor_id, distributor_order.order_id).deliver
            end 
          end
        end  
        
        order.update_attribute(:host,host) 
        o.line_items.each do |item| 
          product = Product.where("product_type ='normal' and lower(description) = ?", item.variant_title.downcase).first  
          product_id = nil 
          product_id = product.id unless product.nil?   
          price = item.price.to_f*100 
          order_item = OrderItem.new(:quantity=>item.quantity, :price=>price, :order_id=>order.id,:product_id=>product_id )  
          order_item.save!   
        end 
      end
    end
  end

  private
  def index_params
    params.permit [].concat(Order.grid_table_strong_params)
  end

  def customer_params
    params.require(:customer).permit(:first_name, :last_name, :email, :address1, :address2, :city, :state, :zip, :country, :billing_first_name, :billing_last_name, :billing_email, :billing_address1, :billing_address2, :billing_city, :billing_state, :billing_zip, :billing_country)
  end

  def order_params
    params.require(:order).permit(:orderer_id, order_items_attributes: [:product_id, :quantity])
  end

  def cc_params
    params.require(:credit_card).permit(:name, :number, :cvc, :exp_month, :exp_year)
  end
end
