class Admin::DistributorsController < Admin::ApplicationController 
  
  respond_to :html
  respond_to :js, only: [:index]

  def index   
    
    respond_to do |format|
      format.html {}
      format.js do
        @distributors = Distributor.all.order("created_at desc")

        grid_table_for(@distributors, index_params)
      end
    end
  end
  
  def set_price
    @distributor = Distributor.find(params[:id])
    @distributor_product_prices = @distributor.distributor_product_prices
  end
  
  def update_price 
    @distributor = Distributor.find(params[:distributor_id]) 
    
    price_18 = 0
    price_38 = 0
    
    params[:distributor_product_prices].each do |p|       
     price_38 = p[:price_38] if p[:price_38]
     price_18 = p[:price_18] if p[:price_18]
    end
    
    params[:distributor_product_prices].each do |p|   
      dpp = @distributor.distributor_product_prices.find p[:id].to_i
      if ["678021720278","678021720292","678021720261","678021720285","678021720308","678021720315","678021720322","678021720339"].include?(dpp.distributor_product.sku.to_s) 
        dpp.update_attribute(:price, price_38.to_f)
      elsif ["713807586669","713807586591","713807586621","713807586607","713807586614","713807586638","713807586645","713807586652"].include?(dpp.distributor_product.sku.to_s) 
        dpp.update_attribute(:price, price_18.to_f)
      else
        dpp.update_attribute(:price, p[:price].to_f) if dpp
      end 
    end
     
    if params[:notify].present? && params[:notify].to_i == 1 
      DistributorMailer.notify_price_change(@distributor).deliver!
    end
    redirect_to admin_distributor_path(@distributor), :notice=>"updated successfully"
  end

  def show
    @distributor = Distributor.find(params[:id])
  end

  def new
    @distributor = Distributor.new
  end

  def create
    @distributor = Distributor.new(distributor_params)

    if @distributor.save
      redirect_to admin_distributor_path(@distributor)
    else
      render :new
    end
  end

  def edit
    @distributor = Distributor.find(params[:id])
  end

  def update
    @distributor = Distributor.find(params[:id])
    @distributor.skip_password_required = true

    if @distributor.update_attributes(distributor_params)
      redirect_to admin_distributor_path(@distributor)
    else
      render :edit
    end
  end

  def approve
    @distributor = Distributor.find(params[:id]) 
    stripe_customer = Stripe::Customer.create(
      email: @distributor.email
    ) 
    password = "hairillusion" 
    if @distributor.update_attributes(password: password, password_confirmation: password, require_password_reset: false, approved: true, stripe_id: stripe_customer.id)
      DistributorMailer.approved(@distributor, password).deliver! 
      redirect_to admin_distributor_path(@distributor)
    else
      render :show
    end
  end

  private
  def index_params
    params.permit [].concat(Distributor.grid_table_strong_params)
  end

  def distributor_params
    distributor_params = params.require(:distributor).permit(:company_name, :first_name, :last_name, :tax_id, :email, :phone, :address1, :tax_info, :address2, :country, :city, :state, :zip, :price)
    distributor_params[:price] = distributor_params[:price].to_f * 100 unless distributor_params[:price].blank?

    distributor_params
  end
end
