class Admin::AgentsController < Admin::ApplicationController  
  before_action :find_agent, only: [:edit, :update, :destroy, :get_orders]
  
  def index
    @agents = Agent.all.order("created_at desc").page(params[:page]).per_page(10) 
  end
  
  def new
    @agent = Agent.new
  end
  
  def create
    @agent = Agent.new(agents_params)

    if @agent.save
      redirect_to '/admin/agents', notice: 'Agent was successfully created.'
    else
      render action: 'new'
    end
  end
  
  def get_orders
    @orders = Order.where("agent_id =?",@agent.id)#.page(params[:page]).per_page(10) 
  end
  
  def get_orders_xls
    @agent = nil
    if params[:agent_id].to_i > 0 
      @agent = Agent.find params[:agent_id]
      @orders = Order.where(:agent_id=>params[:agent_id])
    else
      @orders = Order.where("agent_id is not null")
    end 
    render :pdf => "agent_orders", :layout => 'pdf.html.erb'   
  end

  def update
    if @agent.update(agent_params)
       redirect_to '/admin/agents', notice: 'Agent was successfully updated.'
    else
      render action: 'edit'
    end    
  end
  
  def destroy
    @agent.destroy
    redirect_to admin_agents_url, notice: 'Agent was successfully destroyed.'    
  end  
  
  private 
  
  def find_agent
    @agent = Agent.find params[:id]
  end 

  def agents_params
    params.require(:agent).permit(:name,:email)
  end
      
end
