class UsersController < ApplicationController
  before_filter :check_login
  
  def index
    @customer = Customer.find session[:customer_id]
    @order = Order.where(:orderer_id=>@customer.id, :orderer_type=>"Customer",:cancelled=>false, :parent_order_id=>nil).last
    @card = CustomerCard.find_by_customer_id @customer.id
  end
  
  def update_card_details
    @error =""
    @customer = Customer.find session[:customer_id]
    card = CustomerCard.where("customer_id=?",@customer.id).first
    if card.update_attributes({card_name: params[:name], card_number: params[:number], ccv:params[:cvc], exp_month:params[:exp_month], exp_year:params[:exp_year]})
      @order = Order.where(:orderer_id=>@customer.id, :orderer_type=>"Customer",:cancelled=>false, :parent_order_id=>nil).last
      @order.warning_sent = false
      @order.card_error = false
      @order.next_delivery_date = Date.today if Date.today > @order.next_delivery_date
      @order.save
      
      OrderMailer.card_updated_notification(@customer.id).deliver! 
    else
      @error = "Couldnt update the card info.Pleaes contact support team"
    end
  end
  
  def logout
    @customer = Customer.find session[:customer_id]
    if @customer
      session[:customer_id] = nil
    end
  end
  
  private 
  def check_login
    if session[:customer_id] && !session[:customer_id].blank?
      
    else
      redirect_to '/', :notice=>"Login required."
    end
  end
end
