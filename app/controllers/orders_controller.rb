class OrdersController < ApplicationController
  def new
    @orderer = Customer.new
    @order = @orderer.orders.build(order_items_attributes: [quantity: 1])
    @credit_card = CreditCard.new
  end
  
  def create_agent_order
    session[:order_id] = nil
    @errors = []
    @err_string = ""
    if session[:product_cart]
      unless params[:email].blank?
        @orderer = Customer.where(:email=>params[:email]).first
        if @orderer
          @orderer.first_name = params[:first_name]
          @orderer.last_name = params[:last_name]
          @orderer.email = params[:email]
          @orderer.address1 = params[:address1]
          @orderer.address2 = params[:address2]
          @orderer.city = params[:city]
          @orderer.state = params[:order_state]
          @orderer.country = params[:cb_country]
          @orderer.zip = params[:zip]
          
          if(params[:billing_first_name] && !params[:billing_first_name].blank? ) 
            @orderer.billing_first_name = params[:billing_first_name]
            @orderer.billing_last_name = params[:billing_last_name]
            @orderer.billing_address1 = params[:billing_address1]
            @orderer.billing_address2 = params[:billing_address2]
            @orderer.billing_city = params[:billing_city]
            @orderer.billing_state = params[:billing_state]
            @orderer.billing_country = params[:billing_country]
            @orderer.billing_zip = params[:billing_zip] 
          else 
            @orderer.billing_first_name = params[:first_name]
            @orderer.billing_last_name = params[:last_name]
            @orderer.billing_address1 = params[:address1]
            @orderer.billing_address2 = params[:address2]
            @orderer.billing_city = params[:city]
            @orderer.billing_state = params[:order_state]
            @orderer.billing_country = params[:cb_country] 
            @orderer.billing_zip = params[:zip] 
          end
          
          if @orderer.valid?
            @orderer.save
          else
            @error = @orderer.errors
          end
        else
          @orderer = Customer.new(:first_name=>params[:first_name],:last_name=>params[:last_name],:email=>params[:email], :address1=>params[:address1],
          :address2=>params[:address2],:city=>params[:city], :phone=>params[:phone], :state=>params[:order_state], :country=>params[:cb_country], :zip=>params[:zip])
          
          if(params[:billing_first_name] && !params[:billing_first_name].blank? ) 
            @orderer.billing_first_name = params[:billing_first_name]
            @orderer.billing_last_name = params[:billing_last_name]
            @orderer.billing_address1 = params[:billing_address1]
            @orderer.billing_address2 = params[:billing_address2]
            @orderer.billing_city = params[:billing_city]
            @orderer.billing_state = params[:billing_state]
            @orderer.billing_country = params[:billing_country]
            @orderer.billing_zip = params[:billing_zip] 
          else 
            @orderer.billing_first_name = params[:first_name]
            @orderer.billing_last_name = params[:last_name]
            @orderer.billing_address1 = params[:address1]
            @orderer.billing_address2 = params[:address2]
            @orderer.billing_city = params[:city]
            @orderer.billing_state = params[:order_state]
            @orderer.billing_country = params[:cb_country] 
            @orderer.billing_zip = params[:zip] 
          end
          
          if @orderer.valid?
            @orderer.save
          else 
            @errors = @orderer.errors.full_messages 
            @errors.each do |p| 
              @err_string = @err_string + p.to_s + "," 
            end  
          end
        end
      end
      @processed = false
      @credit_card = CreditCard.new(:name=>"#{params[:first_name]} #{params[:last_name]}", :number=>params[:card_number], :cvc=>params[:cvv],:exp_month=>params[:month],:exp_year=>params[:exp_year])
      
      unless @credit_card.valid?  
        @errors = @credit_card.errors.messages 
        @errors.each do |p|  
          @err_string = @err_string + p[1][0] + "," 
        end  
      end   
       
      if (@errors.nil? || @errors.empty? || @errors == "") && @credit_card.valid?   
        begin 
          if @orderer.stripe_id.blank? 
            stripe_customer = Stripe::Customer.create(email: @orderer.email) 
            @orderer.stripe_id = stripe_customer.id
            @orderer.save
            if session[:product_cart][:type] == "recurrent"  
              #its free for first time
               unit_price = 0
            end 
          end   
          total_price = 0 
          product = nil
          if session[:product_cart]
            if session[:product_cart][:type] == "recurrent" 
              #update here kk
              product = Product.where(:description=>session[:product_cart][:name],:product_type=>'recurrent').first
              if product  
                total_price = 0
              end  
            else
              if session[:product_cart][:products]
                session[:product_cart][:products].each do |p|   
                  price = ( p[:quantity].to_f * p[:price].to_f*100 ) + ( ph_from_session.to_f*100 ) 
                  total_price = total_price + price 
                end 
              end
            end
          end    
          #only p&h fee for recurrent plan 
          total_price = 8.95*100 if session[:product_cart][:type] == "recurrent" 
          if total_price > 0 
            if session[:product_cart][:type] == "recurrent"  
              @order = Order.new(:version_2_order=>true, :orderer_id=>@orderer.id, :orderer_type=>"Customer", :order_type=>"recurrent",:first_delivery_date=>Date.today,:next_delivery_date=>Date.today)  
            else
              @order = Order.new(:version_2_order=>true, :orderer_id=>@orderer.id, :orderer_type=>"Customer", :order_type=>"normal")          
            end 
            #set price to stripe
            total_price = total_price
            charge = Stripe::Charge.create(
               #customer: @orderer.stripe_id,
              amount: total_price.to_i,
              description: session[:product_cart][:type],
              currency: 'usd',
              card: { name: @orderer.name, number:params[:card_number], cvc:params[:cvv], exp_month:params[:month], exp_year:params[:exp_year]}
            ) 
            @order.eh = params[:eh] 
            @order.stripe_id = charge.id 
            @orderer.save!
            @order.agent_id = params[:agent_id] 
            @order.save   
            
            card = CustomerCard.where("customer_id=?",@orderer.id).first
            unless card
              card = CustomerCard.new(:card_name=>@orderer.name, :card_number=>params[:card_number], :ccv=>params[:cvv], :exp_month=>params[:month], :exp_year=>params[:exp_year], :customer_id=>@orderer.id)
              card.save     
            else 
              card.update_attributes({card_name: @orderer.name, card_number: params[:card_number], ccv:params[:cvv], exp_month:params[:month], exp_year:params[:exp_year]})
            end
            if session[:product_cart][:type] == "recurrent"   
              @order.process_handling_price = 8.95 
              @order.shipping_price = 0 
              @order.save  
              order_item = OrderItem.new(:order_id=>@order.id,:tax=>895, :product_id=>product.id,:quantity=>1, :price=>0, :product_type=>"Product") 
              order_item.save(:validate=>false)  
              
              #get free optimizer
              optimizer = Product.where(:description=>"Optimizer").first
              order_item = OrderItem.new(:order_id=>@order.id,:tax=>session[:product_cart][:tax].to_f*100, :product_id=>optimizer.id,:quantity=>1, :price=>0, :product_type=>"Product")
              order_item.save(:validate=>false) 
               
            else  
              @processed = true  
              if session[:product_cart]
                if session[:product_cart][:products]
                  session[:product_cart][:products].each do |p|   
                    product = Product.where("product_type = 'normal'").where(:description=>p[:name]).first 
                    order_item = OrderItem.new(:order_id=>@order.id,:tax=>p[:tax].to_f*100, :product_id=>product.id,:quantity=>p[:quantity], :price=>p[:price].to_f*100, :product_type=>"Product")
                    order_item.save
                  end 
                end
              end     
              @order.update_attribute(:process_handling_price, 0)
              if session[:shipping_price].to_f
                @order.update_attribute(:shipping_price, session[:shipping_price].to_f) 
              else
                @order.update_attribute(:shipping_price, @order.get_total_shipping(@shipping)) 
              end 
            end
            session[:order_id] = @order.id
            render "thank_you"
          end
          
        rescue Stripe::CardError => e 
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err  = body[:error]
          @error = err
          if @order 
            @order.cancelled = true  
            @order.cancelled_at = Time.now
            @order.paid = false 
            @order.save!
          end
          card.destroy if card
          puts "Status is: #{e.http_status}"
          puts "Type is: #{err[:type]}"
          puts "Code is: #{err[:code]}"
          # param is '' in this case
          puts "Param is: #{err[:param]}"
          puts "Message is: #{err[:message]}" 
          flash[:alert] = err[:message] 
          @err_string = err[:message] 
          end 
        end  
      else
        card.destroy if card
      end    
  end
  

  def create_order   
    
    shipping_code = session[:country_shipping_code] rescue ""
    shipping_price = session[:shipping_price] rescue 0 
    
    session[:order_id] = nil
    @errors = []
    @err_string = ""
    if session[:product_cart]
      unless params[:email].blank?
        @orderer = Customer.where(:email=>params[:email]).first
        if @orderer
          @orderer.first_name = params[:first_name]
          @orderer.last_name = params[:last_name]
          @orderer.email = params[:email]
          @orderer.address1 = params[:address1]
          @orderer.address2 = params[:address2]
          @orderer.city = params[:city]
          @orderer.state = params[:order_state]
          @orderer.country = params[:cb_country]
          @orderer.zip = params[:zip]
          
          if(params[:billing_first_name] && !params[:billing_first_name].blank? )
            @orderer.billing_first_name = params[:billing_first_name]
            @orderer.billing_last_name = params[:billing_last_name]
            @orderer.billing_address1 = params[:billing_address1]
            @orderer.billing_address2 = params[:billing_address2]
            @orderer.billing_city = params[:billing_city]
            @orderer.billing_state = params[:billing_state]
            @orderer.billing_country = params[:billing_country]
            @orderer.billing_zip = params[:billing_zip] 
          else 
            @orderer.billing_first_name = params[:first_name]
            @orderer.billing_last_name = params[:last_name]
            @orderer.billing_address1 = params[:address1]
            @orderer.billing_address2 = params[:address2]
            @orderer.billing_city = params[:city]
            @orderer.billing_state = params[:order_state]
            @orderer.billing_country = params[:cb_country] 
            @orderer.billing_zip = params[:zip] 
          end
          
          if @orderer.valid?
            @orderer.save
          else
            @error = @orderer.errors
          end 
        else 
          @orderer = Customer.new(:first_name=>params[:first_name],:last_name=>params[:last_name],:email=>params[:email], :address1=>params[:address1],
          :address2=>params[:address2],:city=>params[:city], :phone=>params[:phone], :state=>params[:order_state], :country=>params[:cb_country], :zip=>params[:zip])
          
          if(params[:billing_first_name] && !params[:billing_first_name].blank? ) 
            @orderer.billing_first_name = params[:billing_first_name]
            @orderer.billing_last_name = params[:billing_last_name]
            @orderer.billing_address1 = params[:billing_address1]
            @orderer.billing_address2 = params[:billing_address2]
            @orderer.billing_city = params[:billing_city]
            @orderer.billing_state = params[:billing_state]
            @orderer.billing_country = params[:billing_country]
            @orderer.billing_zip = params[:billing_zip] 
          else  
            @orderer.billing_first_name = params[:first_name]
            @orderer.billing_last_name = params[:last_name]
            @orderer.billing_address1 = params[:address1]
            @orderer.billing_address2 = params[:address2]
            @orderer.billing_city = params[:city]
            @orderer.billing_state = params[:order_state]
            @orderer.billing_country = params[:cb_country] 
            @orderer.billing_zip = params[:zip] 
          end
             
          
          if @orderer.valid?
            @orderer.save
          else 
            @errors = @orderer.errors.full_messages 
            @errors.each do |p| 
              @err_string = @err_string + p.to_s + "," 
            end  
          end
        end
      end
      @processed = false
      @credit_card = CreditCard.new(:name=>"#{params[:first_name]} #{params[:last_name]}", :number=>params[:card_number], :cvc=>params[:cvv],:exp_month=>params[:month],:exp_year=>params[:exp_year])
      
      unless @credit_card.valid?  
        @errors = @credit_card.errors.messages 
        @errors.each do |p|  
          @err_string = @err_string + p[1][0] + "," 
        end  
      end   
       
      if (@errors.nil? || @errors.empty? || @errors == "") && @credit_card.valid?    
        begin 
          if @orderer.stripe_id.blank? 
            stripe_customer = Stripe::Customer.create(email: @orderer.email) 
            @orderer.stripe_id = stripe_customer.id
            @orderer.save
            if session[:product_cart][:type] == "recurrent"  
              #its free for first time
               unit_price = 0
            end 
          end   
          total_price = 0 
          product = nil
          if session[:product_cart]
            if session[:product_cart][:type] == "recurrent" 
              #update here kk
              product = Product.where(:description=>session[:product_cart][:name],:product_type=>'recurrent').first
              if product  
                total_price = 0
              end  
            else
              if session[:product_cart][:products]
                session[:product_cart][:products].each do |p|   
                  price = ( p[:quantity].to_f * p[:price].to_f*100 ) + ( ph_from_session.to_f*100 ) 
                  total_price = total_price + price 
                end 
              end
            end
          end     
          #only p&h fee for recurrent plan 
          total_price = 8.95*100 if session[:product_cart][:type] == "recurrent"  
          
          if total_price > 0 
            if session[:product_cart][:type] == "recurrent"  
              @order = Order.new(:shipping_code=>shipping_code, :version_2_order=>true, :orderer_id=>@orderer.id, :orderer_type=>"Customer", :order_type=>"recurrent",:first_delivery_date=>Date.today,:next_delivery_date=>Date.today)  
            else
              @order = Order.new(:shipping_code=>shipping_code, :version_2_order=>true, :orderer_id=>@orderer.id, :orderer_type=>"Customer", :order_type=>"normal")          
            end 
            #set price to stripe
            total_price = total_price
            charge = Stripe::Charge.create(
               #customer: @orderer.stripe_id,
              amount: total_price.to_i,
              description: "#{session[:product_cart][:type]}-customer_#{@orderer.id}",
              currency: 'usd',
              card: { name: @orderer.name, number:params[:card_number], cvc:params[:cvv], exp_month:params[:month], exp_year:params[:exp_year]}
            ) 
            @order.stripe_id = charge.id 
            @order.order_source = "website"
            @orderer.save!
            @order.host = request.host  
             
            @order.save   
            
            card = CustomerCard.where("customer_id=?",@orderer.id).first
            unless card
              card = CustomerCard.new(:card_name=>@orderer.name, :card_number=>params[:card_number], :ccv=>params[:cvv], :exp_month=>params[:month], :exp_year=>params[:exp_year], :customer_id=>@orderer.id)
              card.save     
            else 
              card.update_attributes({card_name: @orderer.name, card_number: params[:card_number], ccv:params[:cvv], exp_month:params[:month], exp_year:params[:exp_year]})
            end
            if session[:product_cart][:type] == "recurrent"   
              @order.process_handling_price = 8.95 
              @order.shipping_price = 0
              @order.save  
              order_item = OrderItem.new(:order_id=>@order.id,:tax=>895, :product_id=>product.id,:quantity=>1, :price=>0, :product_type=>"Product") 
              order_item.save(:validate=>false)  
              
              #get free optimizer
              optimizer = Product.where(:description=>"Optimizer").first
              order_item = OrderItem.new(:order_id=>@order.id,:tax=>session[:product_cart][:tax].to_f*100, :product_id=>optimizer.id,:quantity=>1, :price=>0, :product_type=>"Product")
              order_item.save(:validate=>false) 
               
            else  
              @processed = true  
              if session[:product_cart]
                if session[:product_cart][:products]
                  session[:product_cart][:products].each do |p|    
                    product = Product.where("product_type = 'normal'").where(:description=>p[:name]).first 
                    order_item = OrderItem.new(:order_id=>@order.id,:tax=>p[:tax].to_f*100, :product_id=>product.id,:quantity=>p[:quantity], :price=>p[:price].to_f*100, :product_type=>"Product")
                    order_item.save
                  end 
                end
              end    
              @order.update_attribute(:process_handling_price, 0)  
              
              @order.update_attribute(:ship_from_shipwire, !@order.can_ship_from_amazon) 
            
              if shipping_price >0 
                @order.update_attribute(:shipping_price, shipping_price.to_f) 
              else
                @order.update_attribute(:shipping_price, @order.get_total_shipping(@shipping)) 
              end
            end

            logger.info "CCCCC#{total_price.inspect}CCCCCCCCCCCCCCCCCCCCc--#{@held_value.inspect}"
            if total_price > 1800
              @order.held = true
              @order.save
            end
            session[:order_id] = @order.id
            render "thank_you"
          end
          
        rescue Stripe::CardError => e 
          # Since it's a decline, Stripe::CardError will be caught
          body = e.json_body
          err  = body[:error]
          @error = err
          @order.cancelled = true  
          @order.cancelled_at = Time.now
          @order.paid = false 
          @order.save!
          card.destroy if card
          puts "Status is: #{e.http_status}"
          puts "Type is: #{err[:type]}"
          puts "Code is: #{err[:code]}"
          # param is '' in this case
          puts "Param is: #{err[:param]}"
          puts "Message is: #{err[:message]}" 
          flash[:alert] = err[:message] 
          @err_string = err[:message] 
          end 
        end  
      else
        card.destroy if card
      end     
  end
  
  def buy_upsell   
    order = nil
    if session[:order_id]  
      upsell = Product.where(:description=>params[:type]).first  
      if upsell
        order = Order.find session[:order_id]
        if order 
          total_price = upsell.price
          orderer = order.orderer  
          customer_card = CustomerCard.find_by_customer_id orderer.id    
          charge = Stripe::Charge.create( 
            amount: total_price.to_i,
            description: upsell.description,
            currency: 'usd',
            card: { name: customer_card.card_name, number:customer_card.card_number, cvc:customer_card.ccv, 
              exp_month:customer_card.exp_month, exp_year:customer_card.exp_year}
          ) 
          order_item = OrderItem.new(:order_id=>order.id,:tax=>0, :product_id=>upsell.id,:quantity=>1, :price=>upsell.price.to_f, :product_type=>"Product")
          order_item.save
        end 
      end 
      
      order.update_attribute(:ship_from_shipwire, !order.can_ship_from_amazon)  
      if params[:number].to_i == 1 
          render "second_upsell" 
      elsif params[:number].to_i == 2 
        if order.order_type == "recurrent" 
          render "fourth_upsell"
        else
          render "third_upsell"
        end 
      elsif params[:number].to_i == 3 
        if order.order_type == "recurrent" 
          render "final_thanks"
        else
          render "fourth_upsell"
        end          
      elsif params[:number].to_i == 4
        @total_price = order.total_price.to_f + order.shipping_price + order.process_handling_price.to_f
        OrderMailer.order_receipt(session[:order_id]).deliver! 
        OrderMailer.admin_notification(session[:order_id]).deliver!
         
        session[:country_shipping_code] = nil
        session[:shipping_price] = nil 
  
        render "final_thanks"
      end 
    end  
  end 
    
  def next_upsell
    @order = Order.find session[:order_id] 
    if (params[:type] == "Water Resistant Spray" ) 
      OrderMailer.order_receipt(@order).deliver!   
      OrderMailer.admin_notification(session[:order_id]).deliver! 
      
      session[:country_shipping_code] = nil
      session[:shipping_price] = nil  
    end  
    logger.info "ssssss"
    logger.info @order.can_ship_from_amazon
    
    @order.update_attribute(:ship_from_shipwire, !@order.can_ship_from_amazon) 
    
    @total_price =  @order.total_price.to_f + @order.shipping_price.to_f  + @order.process_handling_price.to_f
  end

  def create
    
    if params[:get_shipping].to_s == "true"
      
      distributor = Distributor.find params[:order][:orderer_id]
      if distributor 
        items = []
        address = {}
        @error = ''
        qty = 0 
        
        first_name = distributor.first_name
        last_name = distributor.last_name 
        address = distributor.address1
        address2 = distributor.address2 
        country = distributor.country
        state = distributor.state
        city = distributor.city 
        zip = distributor.zip
      
        products = params[:order][:order_items_attributes]
        
        products.each_with_index do |obj,index|  
          d = obj[1]  
          if d[:quantity].to_i > 0
            qty +=d[:quantity].to_i 
            product = Product.find d[:product_id].to_i
            if product
              items << { :sku =>  product.sku, :quantity => d[:quantity], :commercialInvoiceValue => d[:price].to_f, :commercialInvoiceValueCurrency => 'USD'}
            end
          end
        end
      
        logger.info "..............."
        logger.info items.inspect
        logger.info "......................"
      
        if items.size > 0
           address    = { 
              :email    => params[:email],
              :name    => "#{first_name} #{last_name}",
              :company    => "",
              :address1    => address,
              :address2    => address2,
              :address3    => "",
              :city        => city,
              :state       => state,
              :postalCode  => zip,
              :country    =>  country,
              :phone      => '', 
              :isCommercial    => 0, 
              :isPoBox    => 0
            } 
        end 
       
        logger.info address.inspect
       
        payload = {options: {  currency: "USD", groupBy: "all" }, order: { shipTo: address, items: items }} 
      
        Shipwire.configure do |config|
          config.username = "support@hairillusion.com"
          config.password = "ZZZack!!!"
          config.endpoint = URI::encode('https://api.shipwire.com')
        end 
      
        response = Shipwire::Rate.new.find(payload)   
        logger.info response.inspect
        begin 
          if response.body['error_summary'].blank? 
            options = response.body['resource']['rates'][0]['serviceOptions'] rescue [] 
            @carriers = []
            options.each do |rate|
              obj = rate['shipments'][0]   
              amount = obj['cost']['amount'].to_f
              
              handling = 1.95 + (qty-1)*1.95
              amount = amount + handling 
              amount = sprintf('%.2f', amount)
              @carriers << { shipping_code: obj['carrier']['code'], name: obj['carrier']['description'], deliver_min_date: obj['expectedDeliveryMinDate'].to_date,deliver_max_date: obj['expectedDeliveryMaxDate'].to_date, amount: amount, currency: obj['cost']['currency'] } 
            end  
            
            logger.info @carriers.inspect
          else
            @error = response.body['error_summary']
          end  
        rescue
         @error = "Please enter valid address, If address looks valid, retry to fetch carriers"
        end  
      else
        @error = "Please login as distributor"
      end 
    else 
      @error = ""
      customer_order = false 
      if order_params[:orderer_id]
        @orderer = Distributor.find(order_params[:orderer_id])
        orderer_valid = true 
      end
  
      @order = Order.new(:shipping_code=>params[:shipment_name], :orderer_id=> order_params[:orderer_id], :orderer_type=>"Distributor", :host=>request.host, :hc_order=>false, :version_2_order=>false )
      
      total_qty = 0 
      total_price = 0 
      shipping_price = 0  
      params[:order][:order_items_attributes].each do |p|  
        if p[1][:quantity].to_i > 0
          total_qty += p[1][:quantity].to_i
          dpp = DistributorProductPrice.where("distributor_id=? and distributor_product_id=?", @orderer.id, p[1][:product_id].to_i).first            
          total_price = total_price + p[1][:quantity].to_i*dpp.price
        end
      end 
       
      if params[:shipment_price].present?
        shipping_price = params[:shipment_price].to_f
      else
        if total_qty > 0
          shipping_price = 5.95 + (total_qty-1)
        else
          shipping_price = 0
        end 
      end
      
      if @orderer.country && @orderer.country.downcase == "us"
        shipping_price = 25
      else
        shipping_price = 50        
      end
       
      total_price = total_price + shipping_price  
      total_price = (total_price*100).to_i
  
      @credit_card = CreditCard.new(cc_params)
  
      order_valid = @order.valid?
      credit_card_valid = @credit_card.valid?    
      
      if orderer_valid && order_valid && credit_card_valid && total_qty > 0   
        #begin
          stripe_customer = Stripe::Customer.create(card: cc_params, email: @orderer.email)  
          @orderer.stripe_id = stripe_customer.id   
        
          charge = Stripe::Charge.create(
            customer: @orderer.stripe_id,
            amount: total_price,
            description: "distributor order-id",
            currency: 'usd' 
          )  
          @order.shipping_price = shipping_price
          @order.stripe_id = charge.id 
          @order.save! 
          counter = 0 
          params[:order][:order_items_attributes].each_with_index do |p,index|  
            if p[1][:quantity].to_i > 0
              dpp = DistributorProductPrice.where("distributor_id=? and distributor_product_id=?", @orderer.id, p[1][:product_id].to_i).first
              if dpp 
                counter = counter + 1
                if counter == 1
                  @order.order_items.create(:order_id=>@order.id, :product_id=>p[1][:product_id].to_i, :product_type=>"DistributorProduct", :quantity=>p[1][:quantity].to_i, :price=>dpp.price*100,:s_h_cost=>595)                  
                else
                  order_item = OrderItem.new(:order_id=>@order.id, :product_id=>p[1][:product_id].to_i, :product_type=>"DistributorProduct", :quantity=>p[1][:quantity].to_i, :price=>dpp.price*100,:s_h_cost=>100)
                  order_item.save!           
                end  
                
              end
            end
          end  
          
          OrderMailer.receipt(@order, total_price, shipping_price*100).deliver!
          OrderMailer.send_notification(@order, total_price, shipping_price*100).deliver!   
            
       # rescue Stripe::CardError => e   
       #   @error = e.message
      #  rescue => e
         # Something else happened, completely unrelated to Stripe
       #  @error = e.message      
       # end 
      else    
        unless @credit_card.valid?      
          @credit_card.errors.messages.each do |p,s|     
            @error = "" if @error.nil? 
            @error = @error + s[1] + '\n' if s[1]
            @error = @error + s[0] + '\n' if s[0].include?("Please enter valid cvc")
          end  
          if ["Number Please enter valid card number"].include? @credit_card.errors.full_messages[0]
            @error = @error + "Number Please enter valid card number" + '\n'
          end
        end    
      end   
    end
  end
  
  def buy_product 
  end
  
  def confirmation
  end
  

  private
  
  def ph_from_session
    return 0 unless session[:product_cart]
    price =  session[:shipping_price].to_f rescue "0"
    if price > 0
      return price
    else
      tax = @shipping
      session[:product_cart][:products].each_with_index do |p,index| 
        tax +=  p[:quantity].to_f*1
      end
      return tax - 1       
    end

  end
  def customer_params
    params.require(:customer).permit(:first_name, :last_name, :email, :address1, :address2, :city, :state, :zip, :country)
  end

  def order_params
    params.require(:order).permit(:orderer_id, order_items_attributes: [:product_id, :quantity])
  end

  def cc_params
    params.require(:credit_card).permit(:name, :number, :cvc, :exp_month, :exp_year)
  end
end